
ZHONGLI_LEVEL_90_HEALTH = 14695
ZHONGLI_LEVEL_90_BASE_ATTACK = 251
ZHONGLI_LEVEL_90_GEO_BONUS = 0.288
FAVONIUS_LANCE_LEVEL_90_ATTACK = 565

# Note: Use real multiplier here rather than percentage since we are comparing different stats.
ZHONGLI_BURST_ATTACK_SCALING_DICT = {6:6.3956, 8:7.6964, 10:8.9972}
ZHONGLI_BURST_HP_SCALING = 0.33 / 1.754 # Note: Hacky modification to account for geo damage not being applied apparently?

BASE_ATTACK = ZHONGLI_LEVEL_90_BASE_ATTACK + FAVONIUS_LANCE_LEVEL_90_ATTACK

from artifact_information import FEATHER_ATTACK, ARTIFACT_ATTACK_PERCENTAGE, ARTIFACT_ELEMENTAL_DAMAGE_BONUS, ARTIFACT_CRIT_RATE, FLOWER_HEALTH
ARTIFACT_HP_PERCENTAGE = 0.466

from utils import calculate_crit_multiplier

def simulate(burst_level=6, substat_crit_rate=0.25, substat_crit_damage=0.50+0.40, substat_attack=100, substat_health=598):
    burst_attack_multiplier = ZHONGLI_BURST_ATTACK_SCALING_DICT[burst_level]
    
    # --- Crit Rate attack build ---
    print "Crit Rate attack:", ((BASE_ATTACK * (1 + ARTIFACT_ATTACK_PERCENTAGE) + substat_attack + FEATHER_ATTACK) * burst_attack_multiplier + (ZHONGLI_LEVEL_90_HEALTH + FLOWER_HEALTH + substat_health) * ZHONGLI_BURST_HP_SCALING) * \
          (1 + ZHONGLI_LEVEL_90_GEO_BONUS + ARTIFACT_ELEMENTAL_DAMAGE_BONUS) * calculate_crit_multiplier(substat_crit_rate+ARTIFACT_CRIT_RATE, substat_crit_damage)
    
    # --- Crit Rate Health ---
    print "Crit Rate Health:", ((BASE_ATTACK + substat_attack + FEATHER_ATTACK) * burst_attack_multiplier + (ZHONGLI_LEVEL_90_HEALTH * (1 + ARTIFACT_HP_PERCENTAGE) + FLOWER_HEALTH + substat_health)  * ZHONGLI_BURST_HP_SCALING) * \
          (1 + ZHONGLI_LEVEL_90_GEO_BONUS + ARTIFACT_ELEMENTAL_DAMAGE_BONUS) * calculate_crit_multiplier(substat_crit_rate+ARTIFACT_CRIT_RATE, substat_crit_damage)
    
    # --- 2 Health ---
    print "2 Health", ((BASE_ATTACK + substat_attack + FEATHER_ATTACK) * burst_attack_multiplier + (ZHONGLI_LEVEL_90_HEALTH * (1 + ARTIFACT_HP_PERCENTAGE * 2) + FLOWER_HEALTH + substat_health)  * ZHONGLI_BURST_HP_SCALING) * \
          (1 + ZHONGLI_LEVEL_90_GEO_BONUS + ARTIFACT_ELEMENTAL_DAMAGE_BONUS) * calculate_crit_multiplier(substat_crit_rate, substat_crit_damage)
    
    # --- 3 Health ---
    print "3 Health", ((BASE_ATTACK + substat_attack + FEATHER_ATTACK) * burst_attack_multiplier + (ZHONGLI_LEVEL_90_HEALTH * (1 + ARTIFACT_HP_PERCENTAGE * 3) + FLOWER_HEALTH + substat_health)  * ZHONGLI_BURST_HP_SCALING) * \
          (1 + ZHONGLI_LEVEL_90_GEO_BONUS) * calculate_crit_multiplier(substat_crit_rate, substat_crit_damage)
    
def print_hp_amounts():
    print "\n---HP amount (without substats)---"
    
    for i in range(4):
        print i, "HP artifacts:", ZHONGLI_LEVEL_90_HEALTH * (1 + ARTIFACT_HP_PERCENTAGE * i) + FLOWER_HEALTH
    
def main():
    for burst_level in [6, 8, 10]:
        for substat_crit_rate, substat_crit_damage in [(0.10, 0.20), (0.20, 0.40), (0.40, 0.80), (0.60, 1.20)]:
            print "\n---burst_level:", burst_level, "substat_crit_rate:", substat_crit_rate, "substat_crit_damage:", substat_crit_damage, "---"
            simulate(burst_level, substat_crit_rate, substat_crit_damage)
            
    print_hp_amounts()
    
if __name__ == "__main__":
    main()