
def balance_stats(attack_percentage, crit_rate, crit_damage, minimum_attack_percentage=0.0, precision=7):
    # Optimal attack formula: AtkBonus = 3*(1+CR*CD)/(CD+2*CR) - 1
    # From: https://old.reddit.com/r/Genshin_Impact/comments/k82g64/the_best_crit_to_atk_ratio_thorough_math/
    
    balanced_crit_rate, balanced_crit_damage = balance_crit(crit_rate, crit_damage)
    
    # ..... estimate the ideal attack for crit by binary search style trial and error because I can't math.
    
    for i in xrange(1000): # Maximium iterations in case something goes wrong and an infinite loop would happen.
        ideal_attack_for_crit = 3.0 * (1+balanced_crit_rate*balanced_crit_damage)/(balanced_crit_damage +2*balanced_crit_rate) - 1
        
        previous_attack_percentage = attack_percentage
        
        difference = abs(ideal_attack_for_crit - attack_percentage)
        
        if ideal_attack_for_crit > attack_percentage:
            attack_percentage = attack_percentage + difference / 2.0
            balanced_crit_rate = balanced_crit_rate - difference / 2.0 / 3.0
            balanced_crit_damage = balanced_crit_damage - difference / 2.0 / 3.0 * 2.0
        else:
            attack_percentage = attack_percentage - difference / 2.0
            balanced_crit_rate = balanced_crit_rate + difference / 2.0 / 3.0
            balanced_crit_damage = balanced_crit_damage + difference / 2.0 / 3.0 * 2.0
        
        # Stop conditions
        if round(previous_attack_percentage, precision) == round(attack_percentage, precision): # When we reach the precision limit...
            break
        
    if i == 999:
        print "Warning: Max iterations reached during stat balancing."
        
    # If value is less than minimum attack, use minimum attack (and move extra stats from crit)
    if attack_percentage < minimum_attack_percentage:
        difference = minimum_attack_percentage - attack_percentage
        
        attack_percentage = minimum_attack_percentage
        
        balanced_crit_rate = balanced_crit_rate - difference / 3.0
        balanced_crit_damage = balanced_crit_damage - difference / 3.0 * 2.0
        
    return round(attack_percentage, precision), round(balanced_crit_rate, precision), round(balanced_crit_damage, precision)
    
def balance_crit(crit_rate, crit_damage):    
    difference = abs(crit_rate - (crit_damage / 2.0))
    
    if crit_rate > crit_damage / 2.0:
        balanced_crit_rate = crit_rate - difference / 2.0
        balanced_crit_damage = crit_damage + difference
    else:
        balanced_crit_rate = crit_rate + difference / 2.0
        balanced_crit_damage = crit_damage - difference
    
    return balanced_crit_rate, balanced_crit_damage
    
def test():
    '''These should all give the same result.'''
    print balance_stats(1.25, 1.0, 2)
    
    print balance_stats(1.25, 1.5, 1)
    
    print balance_stats(1.40, 0.90, 2.0)
    
    print balance_stats(1.40, 1.0, 1.8)
    
    print balance_stats(1.40, 0.95, 1.9)    
    
def test2():
    '''Comparing formula to ideal stats document'''
    
    # Memory of Dust... gives a value below the minimum possible on Memory of Dust.
    print (212 + 608) * (1 + (3*(1+0.720*1.440)/(1.440+2*0.72) - 1))
    
    # Lost prayer matches
    print (212 + 608) * (1 + (3*(1+0.840*1.68)/(1.68+2*0.84) - 1))  
    
    print (212 + 608) * (1 + (3*(1+1.2234*2.4468)/(2.4468+2*1.2234) - 1)) 
    
def test3():
    '''Check that stats going in are equivalent to stats coming out.'''
    
    print balance_stats(1.21187, 0.427+0.331, 1.244, 0.845268)
    
    print 1.21187 / 1.5 + 0.427+0.331 / 1 + 1.244 / 2 
    
    print 1.1216702 / 1.5 + 0.7200666 / 1 + 1.4401332 / 2
    
    
if __name__ == "__main__":
    #test()
    
    #test2()
    
    test3()
    