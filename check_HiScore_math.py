
def basic_reproduction():
    # Using talent 6. (not 1)
    B46 = 78.4 # Normals
    B47 = 244 # Charged
    B48 = 69.4 # Starjade
    B93 = 0.5 # Geo damage bonus
    B98 = 0.4 # Retracing bolide damage bonus
    B52 = 323 # Skill damage
    B59 = 1464 # Ultimate damage
    B92 = 637 # Base attack
    B94 = 700 # Flat attack
    B95 = 0.6 # Percent attack
    
    print "Original:", (((B46*8)*(1+B93+B98)+(((B47*8+B48*8)*(1+B98+B93))+((B52+B59)*(1+B93)))*(B92+B94+(B92*B95)))/100)
    print "Fixed:", ((((B46*8)*(1+B93+B98)+((B47*8+B48*8)*(1+B98+B93))+((B52+B59)*(1+B93)))*(B92+B94+(B92*B95)))/100)

def readable():
    # Using talent 6. (not 1)
    normal_scaling = 78.4 # Normals
    charged_scaling = 244 # Charged
    starjade_scaling = 69.4 # Starjade
    geo_damage_bonus = 0.5 # Geo damage bonus
    bolide_damage_bonus = 0.4 # Retracing bolide damage bonus
    skill_scaling = 323 # Skill damage
    ultimate_scaling = 1464 # Ultimate damage
    base_attack = 637 # Base attack
    flat_attack = 700 # Flat attack
    percent_attack = 0.6 # Percent attack    
    
    normal_damage = (normal_scaling*8)*(1+geo_damage_bonus+bolide_damage_bonus)
    charged_damage = ((charged_scaling*8+starjade_scaling*8)*(1+bolide_damage_bonus+geo_damage_bonus))
    skill_damage = ((skill_scaling+ultimate_scaling)*(1+geo_damage_bonus))
    attack = (base_attack+flat_attack+(base_attack*percent_attack))
    
    print "\nnormal_damage:", normal_damage
    print "charged_damage:", charged_damage
    print "skill_damage:", skill_damage
    print "attack:", attack
    print "attack bonus:", attack / base_attack - 1.0
    
    # It took me way too long to notice what's weird with this formula lmao
    print ((normal_damage+(charged_damage+skill_damage)*attack)/100)

def debug_print(n_normal_charged_combos, normal_scaling, charged_scaling, jade_screen_scaling, starshatter_scaling, jade_star_scaling, atk_bonus, dmg_bonus, 
                normal_bonus, charged_bonus, skill_bonus, ult_bonus):
    #base_attack=627
    base_attack = 720
    
    normal_damage = normal_scaling * (1 + dmg_bonus + normal_bonus) * n_normal_charged_combos
    charged_damage = charged_scaling * (1 + dmg_bonus + charged_bonus) * n_normal_charged_combos
    jade_star_damage = jade_star_scaling * (1 + dmg_bonus + charged_bonus) * n_normal_charged_combos
    skill_damage = jade_screen_scaling * (1 + dmg_bonus + skill_bonus)
    ult_damage = starshatter_scaling * (1 + dmg_bonus + ult_bonus)
    attack = base_attack * (1 + atk_bonus)
    
    print "normal:", normal_damage
    print "charged:", charged_damage
    print "jade_star_damage:", jade_star_damage
    print "charged+jade_star_damage:", charged_damage+jade_star_damage
    print "skill:", skill_damage
    print "ult damage:", ult_damage
    print "skill+ult:", skill_damage + ult_damage
    print "attack:", attack
    print "attack_bonus:", atk_bonus
    print "elemental_damage_bonus:", dmg_bonus
    print "normal_scaling:", normal_scaling
    print "charged_scaling:", charged_scaling
    print "jade_star_scaling:", jade_star_scaling
    print "n_normal_charged_combos:", n_normal_charged_combos
    print "jade_screen_scaling:", jade_screen_scaling
    print "starshatter_scaling:", starshatter_scaling
    
    print ""
    print "Formula:", ((normal_damage+charged_damage+jade_star_damage+skill_damage+ult_damage)*attack)/100    

def analysis1():
    '''Problem was resolved.'''
    basic_reproduction()
    readable()    
    
def reproduction2():
    # Talent levels are 7. Memory of Dust only.
    H46 = 84 # Normal
    H202 = 0.8
    H47 = 261 # Charge
    H48 = 74.4 # Starjade
    H52 = 346 # Skill damage
    H59 = 1560 # Skill damage * 2
    
    H197 = 720 # Base attack
    H199 = 400 # Flat attack bonus
    L189 = 0.8 # Attack bonus
    B167 = 0.4 # MoD passive bonus
    G165 = 0.453 # MoD attack substat
    H200 = 0.5 # Crit rate
    H201 = 1.5 # Crit damage
    
    B198 = (H197+H199+(H197*(L189+B167+G165+0.18)))*(1+(H200*H201))
    fixed_B198 = (H197+H199+(H197*(L189+B167+G165+0.18)))*(1+(H200*H201))
    
    print "Original:", (((H46*(1+H202))+(H47*(1+H202))+(H48*(1+H202))+((H52)*(1+H202))+(H59*(1+H202)))*B198)/100 
    print "Fixed:", (((H46*(1+H202))+(H47*(1+H202))+(H48*(1+H202))+((H52)*(1+H202))+(H59*(1+H202)))*fixed_B198)/100 

def readable2():
    normal_scaling = 84 # Normal
    elemental_damage_bonus = 0.8 # Elemental damage bonus
    charged_scaling = 261 # Charge
    starjade_scaling = 74.4 # Starjade
    skill_scaling = 346 # Skill damage
    skill_scaling_2 = 1560 # Skill damage * 2
    
    base_attack = 720.0 # Base attack
    flat_attack = 400.0 # Flat attack bonus
    attack_bonus = 0.8 # Attack bonus
    mod_passive_bonus = 0.4 # MoD passive bonus
    mod_attack_substat = 0.453 # MoD attack substat
    crit_rate = 0.5 # Crit rate
    crit_damage = 1.5 # Crit damage
    
    crit_multiplier = (1+(crit_rate*crit_damage))
    
    attack = (base_attack+flat_attack+(base_attack*(attack_bonus+mod_passive_bonus+mod_attack_substat+0.18))) * crit_multiplier
    print "base_attack:", base_attack
    print "attack_bonus", attack_bonus
    print "ATTACK CHECK:", (flat_attack/base_attack) + (attack_bonus) + 0.18, mod_passive_bonus, mod_attack_substat
    
    print "attack:", attack
    
    normal_damage = (normal_scaling*(1+elemental_damage_bonus))
    charged_damage = (charged_scaling*(1+elemental_damage_bonus))
    starjade_damage = (starjade_scaling*(1+elemental_damage_bonus))
    skill_damage = ((skill_scaling)*(1+elemental_damage_bonus))
    ult_damage = (skill_scaling_2*(1+elemental_damage_bonus))
    
    print "\nnormal_damage:", normal_damage
    print "charged_damage:", charged_damage
    print "starjade_damage:", starjade_damage
    print "skill_damage:", skill_damage
    print "ult_damage:", ult_damage
    print "attack:", attack
    print "attack bonus:", attack / base_attack - 1.0  
    print "elemental_damage_bonus:", elemental_damage_bonus
    print "normal_scaling:", normal_scaling
    print "charged_scaling:", charged_scaling
    print "jade_star_scaling:", starjade_scaling
    print "jade_screen_scaling:", skill_scaling
    print "starshatter_scaling:", skill_scaling_2
    print "crit_multiplier:", crit_multiplier
    #print ""
    
    multiplier = (normal_damage+charged_damage+starjade_damage+skill_damage+ult_damage)
    
    print "Multiplier:", multiplier
    print "Multiplier*attack_bonus:", multiplier * (attack / base_attack)
    print "Result:", (multiplier*attack)/100  
        
def analysis2():
    reproduction2()
    readable2()
        
if __name__ == "__main__":
    #analysis1()
    
    analysis2()