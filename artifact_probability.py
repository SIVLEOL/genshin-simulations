import random

def simulate(correct_set_odds=0.5, useful_pieces_wanted="all", useful_number_wanted=4, allow_attack_hat=False, allow_crit_damage_hat=False):
    types = ["hat", "timepiece", "cup", "feather", "flower"]
    
    if useful_pieces_wanted == "all":
        useful_pieces_wanted = types
    
    useful_odds = {"hat":0.1428571, "timepiece":0.2, "cup":0.1, "feather":1.0, "flower":1.0}
    
    if allow_attack_hat:
        useful_odds["hat"] += 0.1428571
    
    if allow_crit_damage_hat:
        useful_odds["hat"] += 0.1428571
        
    collected_types = set()
        
    for i in range(1000):
        if not random.randint(0,99) / 100.0 < correct_set_odds:
            continue
        
        type_choice = random.choice(types)
        
        if random.randint(0, 99) / 100.0 < useful_odds[type_choice]:
            if type_choice in useful_pieces_wanted:
                collected_types.add(type_choice)
            
            if len(collected_types) == useful_number_wanted:
                break
    
    return i+1
    
def simulate_many(n, useful_number_wanted=4, **kwargs):
    total_rolls = 0.0
    
    kwargs["useful_number_wanted"] = useful_number_wanted
    
    for i in range(n):
        total_rolls += simulate(**kwargs)
        
    print "Average rolls for", useful_number_wanted, "useable pieces:", total_rolls / n
    print "Average resin used:", 20.0 * total_rolls / n
    
if __name__ == "__main__":
    #simulate_many(1000, correct_set_odds=0.5, useful_number_wanted=2, allow_attack_hat=False, allow_crit_damage_hat=False)
    
    simulate_many(1000, correct_set_odds=0.5, useful_pieces_wanted=["hat"], useful_number_wanted=1, allow_attack_hat=False, allow_crit_damage_hat=False)