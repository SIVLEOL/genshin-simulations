NORMAL_LEVEL_6 = 64.47 + 66.35 + 83.94 + 90.26 + 47.75 + 48.4 + 118.19
BLOOD_BLOSSOM_LEVEL_6 = 89.6
PARAMITA_PAPILIO_LEVEL_6 = 0.0506

SECONDS_PER_COMBO = 3.0

HU_TAO_BASE_ATTACK_90 = 106
HU_TAO_CRIT_DAMAGE_BONUS = 0.884
HU_TAO_BASE_HP_90 = 15552

SECONDS_PER_COMBO = 3.0

from utils import calculate_crit_multiplier

def calculate_combo_multiplier(atk_bonus=1.5, pyro_dmg_bonus=0.466, normal_charged_bonus=0.35, skill_bonus=0.0, ult_bonus=0.0, skill_active=True, low_hp=True, printing=False):
    if low_hp:
        ult_level_6 = 499.4
        pyro_dmg_bonus += 0.33
    else:
        ult_level_6 = 399.52 
        
    if skill_active:
            normal_charged_bonus += pyro_dmg_bonus        
    
    normal_dps = NORMAL_LEVEL_6 * (1 + atk_bonus) * (1 + normal_charged_bonus) / SECONDS_PER_COMBO
    
    skill_dps = 89.6 * (1 + atk_bonus) * (1 + pyro_dmg_bonus + skill_bonus) / 4.0
    
    ult_dps = ult_level_6 * (1 + atk_bonus) * (1 + pyro_dmg_bonus + ult_bonus) / 15.0
    
    effective_combo_multiplier = normal_dps + skill_dps + ult_dps

    if printing:
        print effective_combo_multiplier

    return effective_combo_multiplier    

def get_crescent_pike_stats(refinement=1):
    base_attack = 565
    
    physical_bonus = 0.345
    hit_bonus = 40 + 5 * (refinement-1)
    
    return base_attack, physical_bonus, hit_bonus

def simulate(artifact_attack_percentage=0.878, artifact_hp=4780, artifact_hp_percentage=0.466, artifact_crit_rate=0.453, artifact_crit_damage=0.50+0.387, 
             pyro_dmg_bonus=0.466, crescent_pike_refinement=1):
    
    hp = HU_TAO_BASE_HP_90 * (1 + artifact_hp_percentage) + artifact_hp 
    
    # --- Crescent Pike ---
    
    base_attack, physical_bonus, hit_bonus = get_crescent_pike_stats(crescent_pike_refinement)
    base_attack += HU_TAO_BASE_ATTACK_90
    
    skill_uptime = 9.0 / 16.0
    #skill_uptime = 1.0
    
    attack_bonus = (hp * PARAMITA_PAPILIO_LEVEL_6) / base_attack * skill_uptime
    
    print "Crescent Pike:", calculate_damage_score(base_attack, artifact_attack_percentage, artifact_crit_rate, artifact_crit_damage, attack_bonus=attack_bonus,
                                                   weapon_passive_multiplier=hit_bonus * (1 + artifact_attack_percentage+attack_bonus) * (1 + physical_bonus) * 7 / SECONDS_PER_COMBO)
    
def calculate_damage_score(base_attack, artifact_attack_percentage, artifact_crit_rate, artifact_crit_damage, 
                           attack_bonus=0.0, crit_rate_bonus=0.0, crit_damage_bonus=0.0, weapon_passive_multiplier=0.0, **kwargs):
    damage_score = base_attack * (calculate_combo_multiplier(atk_bonus=artifact_attack_percentage + attack_bonus, **kwargs) + weapon_passive_multiplier) / 100.0 \
        * calculate_crit_multiplier(artifact_crit_rate+crit_rate_bonus, artifact_crit_damage+crit_damage_bonus+HU_TAO_CRIT_DAMAGE_BONUS)
    
    return damage_score        
    
def main():
    simulate(crescent_pike_refinement=1)
    
if __name__ == "__main__":
    main()