# 5* character odds: https://old.reddit.com/r/Genshin_Impact/comments/jvmnac/5_character_banner_pity_analysis_true_rate_of_324/
from random import randint

def simulate_character_banner(n=1000):
    total_number_of_rolls = 0
    total_banner_SSR_count = 0
    total_offbanner_SSR_count = 0
    
    for trial_number in xrange(n):
        number_of_rolls, banner_SSR_count, off_banner_SSR_count = simulate_character_banner_once()
        
        total_number_of_rolls += number_of_rolls
        total_banner_SSR_count += banner_SSR_count
        total_offbanner_SSR_count += off_banner_SSR_count
        
    print "Average rolls for C6 banner unit:", total_number_of_rolls / float(n)
    print "Average primogems for C6 banner unit:", total_number_of_rolls / float(n) * 160
    print "Average banner SSR:", total_banner_SSR_count / float(n)
    print "Average offbanner SSR:", total_offbanner_SSR_count / float(n)
        
def simulate_character_banner_once():
    number_of_rolls = 0
    pity_counter = 0
    banner_pity = False
    banner_SSR_count = 0
    off_banner_SSR_count = 0

    while True:
        pity_counter += 1
        
        roll = randint(0, 999) / 10.0
        number_of_rolls += 1
        
        if pity_counter == 90:
            odds = 100
        elif pity_counter < 74:
            odds = 0.6
        else:
            odds += 5.845
            
        if roll < odds:
            if banner_pity or randint(0,1):
                banner_SSR_count += 1
                banner_pity = False
                
                if banner_SSR_count == 7:
                    break
            else:
                off_banner_SSR_count += 1
                banner_pity = True
                
            pity_counter = 0
            
    return number_of_rolls, banner_SSR_count, off_banner_SSR_count
    
def simulate_weapon_banner(n=1000):
    total_number_of_rolls = 0
    total_banner_SSR_1_count = 0
    total_banner_SSR_2_count = 0
    total_offbanner_SSR_count = 0    
    
    for trial_number in xrange(n):
        number_of_rolls, banner_SSR_1_count, banner_SSR_2_count, off_banner_SSR_count = simulate_weapon_banner_once()
        
        total_number_of_rolls += number_of_rolls
        total_banner_SSR_1_count += banner_SSR_1_count
        total_banner_SSR_2_count += banner_SSR_2_count
        total_offbanner_SSR_count += off_banner_SSR_count
        
    print "Average rolls for R5 banner weapon:", total_number_of_rolls / float(n)
    print "Average primogems for R5 banner weapon:", total_number_of_rolls / float(n) * 160
    print "Average banner SSR weapon 1:", total_banner_SSR_1_count / float(n)
    print "Average banner SSR weapon 2:", total_banner_SSR_2_count / float(n)
    print "Average offbanner SSR:", total_offbanner_SSR_count / float(n)
    
def simulate_weapon_banner_once():
    number_of_rolls = 0
    pity_counter = 0
    banner_pity = False
    banner_SSR_1_count = 0
    banner_SSR_2_count = 0
    off_banner_SSR_count = 0
    epitomized_pity = False
    
    while True:
        pity_counter += 1

        roll = randint(0, 999) / 10.0
        number_of_rolls += 1

        if pity_counter == 80:
            odds = 100
        elif pity_counter < 66:
            odds = 0.6
        else:
            odds = 32.4

        if roll < odds:
            if banner_pity or randint(0,99) < 75:
                banner_pity = False
                
                if epitomized_pity == 2 or randint(0,1):
                    banner_SSR_1_count += 1
                
                    epitomized_pity = 0
                
                    if banner_SSR_1_count == 5:
                        break
                else:
                    banner_SSR_2_count += 1
                    
                    epitomized_pity += 1
                
                    if banner_SSR_2_count == 5:
                        break
            else:
                off_banner_SSR_count += 1
                banner_pity = True

            pity_counter = 0

    return number_of_rolls, banner_SSR_1_count, banner_SSR_2_count, off_banner_SSR_count

if __name__ == "__main__":
    simulate_character_banner()
    simulate_weapon_banner()