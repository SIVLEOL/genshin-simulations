

def calculate_every_other(main_dps_speed=117, bronya_speed=150, dps_skill_speed_mod=10, bronya_advance_forward=18, 
                          loops=9, main_dps_base_speed=98):
    dps_action_value = (10000 / main_dps_speed) 
    bronya_action_value = (10000 / bronya_speed)    
    
    dps_current_action_value = dps_action_value
    bronya_current_action_value = bronya_action_value
    bronya_turn = 1
    first_dps_turn = True
    
    bronya_action_forward_multiplier = 1 - (bronya_advance_forward / 100.0)
    
    for i in range(loops):
        if dps_current_action_value < bronya_current_action_value:
            print "DPS", dps_current_action_value
            
            if first_dps_turn:
                dps_action_value = (dps_action_value * main_dps_speed) / (main_dps_base_speed * (1.0 + dps_skill_speed_mod / 100.0) + (main_dps_speed-main_dps_base_speed))
                first_dps_turn = False
            
            dps_current_action_value += dps_action_value
        else:
            if bronya_turn % 2 == 0:        
                print "Bronya skill", bronya_current_action_value
                print "DPS", bronya_current_action_value
                    
                dps_current_action_value = bronya_current_action_value + dps_action_value  
                bronya_current_action_value += bronya_action_value
            else:
                print "Bronya", bronya_current_action_value
                bronya_current_action_value += bronya_action_value * bronya_action_forward_multiplier # Normal attack action forwards
                
            bronya_turn += 1
    
if __name__ == "__main__":
    #calculate_every_other(main_dps_speed=117.1, bronya_speed=166, dps_skill_speed_mod=10, bronya_advance_forward=24)
    
    #calculate_every_other(main_dps_speed=100, bronya_speed=166, dps_skill_speed_mod=10, bronya_advance_forward=24)
    calculate_every_other(main_dps_speed=100, bronya_speed=168, dps_skill_speed_mod=10, bronya_advance_forward=30)