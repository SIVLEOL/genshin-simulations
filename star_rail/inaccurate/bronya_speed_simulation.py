'''
DO NOT USE

This script has some bugs in speed calculation that make it not accurate.
'''


import sys

from cStringIO import StringIO
from collections import OrderedDict

def simulate(main_dps_speed=101, bronya_speed=99, use_skill_threshold=70, max_action_value=1050, skill_point_generation_per_turn=2, 
             initial_dps_action_value_modifier=0, initial_bronya_action_value_modifier=0, bronya_action_forward_multiplier=0.7, print_details=True):
    '''
    Simulate one set of parameters.
    
    Note: First turn is 150 action value long.'''
    
    if main_dps_speed == 100:
        raise ValueError("Error: main_dps_speed doesn't simulate correctly, so it is not allowed.")
    
    if bronya_speed < 99:
        raise ValueError("Error: Bronya's base speed must be at least 99.")
    
    current_skill_points = 3
    
    skill_point_usage = 0
    dps_action_count = 1
    bronya_skill_count = 0
    
    dps_action_value = (10000 / main_dps_speed) - initial_dps_action_value_modifier
    bronya_action_value = (10000 / bronya_speed) - initial_bronya_action_value_modifier
    
    dps_current_action_value = dps_action_value
    bronya_current_action_value = bronya_action_value
    
    next_sp_generate_turn = dps_action_count
    
    for i in range(1000):
        if dps_current_action_value >= max_action_value:
            break
        elif dps_current_action_value >= next_sp_generate_turn:
            next_sp_generate_turn = dps_current_action_value + 100
            current_skill_points += skill_point_generation_per_turn
        
        if print_details:
            print "sp:", current_skill_points
        
        if bronya_current_action_value < dps_current_action_value:
            if current_skill_points >= 4 and (dps_current_action_value - bronya_current_action_value > use_skill_threshold and current_skill_points > 1):
                skill_point_usage += 1
                current_skill_points = current_skill_points - 1
                bronya_skill_count += 1
                if print_details:
                    print "Bronya skill", bronya_current_action_value
                
                skill_point_usage += 1
                current_skill_points = current_skill_points - 1
                dps_action_count += 1          
                if print_details:
                    print "DPS", bronya_current_action_value
                
                dps_current_action_value = bronya_current_action_value + dps_action_value
                bronya_current_action_value += bronya_action_value
            else:
                skill_point_usage = skill_point_usage - 1
                current_skill_points = current_skill_points + 1
                if print_details:
                    print "Bronya", bronya_current_action_value
                bronya_current_action_value += bronya_action_value * bronya_action_forward_multiplier # Normal attack action forwards
        else:
            skill_point_usage += 1
            current_skill_points = current_skill_points - 1
            dps_action_count += 1
            if print_details:
                print "DPS", dps_current_action_value
            dps_current_action_value += dps_action_value
            
        if current_skill_points < 0:
            raise RuntimeError("Error: Ran out of skill points.")        
            
    print "\nmain_dps_speed=%d, bronya_speed=%d, use_skill_threshold=%d, max_action_value=%d, sp_per_turn=%d" % (main_dps_speed, bronya_speed, use_skill_threshold, max_action_value,
                                                                                                                 skill_point_generation_per_turn)
    print "skill_point_usage:", skill_point_usage
    print "dps_action_count:", dps_action_count
    print "bronya_skill_count:", bronya_skill_count
    print "final_skill_points:", current_skill_points
    
    return dps_action_count, skill_point_usage
    
def simulate_best_skill_threshold(intervals=1, print_more_details=False, **kwargs):
    '''Try all the different possible skill thresholds to use skill at all.'''
    
    use_skill_threshold = 1
    
    old_stdout = sys.stdout
    
    best_dps_action_count = 0
    best_stdout = None
    best_skill_point_usage = 9999
    
    skill_threshold_dps_action_count_dict = OrderedDict()
    
    for i in range(1000):
        if use_skill_threshold > 99:
            break
            
        sys.stdout = temp_stdout = StringIO()
        
        dps_action_count, skill_point_usage = simulate(use_skill_threshold=use_skill_threshold, **kwargs)
            
        skill_threshold_dps_action_count_dict[use_skill_threshold] = dps_action_count
            
        if dps_action_count > best_dps_action_count or (dps_action_count == best_dps_action_count and skill_point_usage <= best_skill_point_usage):
            best_dps_action_count = dps_action_count
            best_stdout = temp_stdout
            best_skill_point_usage = skill_point_usage
            
        use_skill_threshold += intervals
    
    sys.stdout = old_stdout
    
    print best_stdout.getvalue(),
    
    if print_more_details:
        print "skill_threshold_dps_action_count_dict:", skill_threshold_dps_action_count_dict
    
def simulate_best_skill_threshold_multiple_speed(speeds=None, **kwargs):
    '''Try multiple use_skill_threshold with multiple speeds.'''
    
    if speeds is None:
        speeds = [99]
    
    for speed in speeds:
        simulate_best_skill_threshold(print_details=False, bronya_speed=speed, **kwargs)
    
if __name__ == "__main__":
    #simulate(main_dps_speed=151, bronya_speed=118, use_skill_threshold=99, max_action_value=1050)
    
    #simulate_best_skill_threshold(main_dps_speed=105, bronya_speed=124, max_action_value=1050)
    
    #simulate_best_skill_threshold_multiple_speed(main_dps_speed=115, max_action_value=1050, speeds=[120, 124, 126, 128, 130], skill_point_generation_per_turn=2)
    
    simulate_best_skill_threshold_multiple_speed(main_dps_speed=151, max_action_value=1050, speeds=[100+i*5 for i in range(20)], initial_dps_action_value_modifier=-31,
                                                 skill_point_generation_per_turn=1.5, bronya_action_forward_multiplier=0.7)
    
    