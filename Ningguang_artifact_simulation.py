NORMAL_MV_DICT = {1: 28.0, 2: 30.1, 3: 32.2, 4: 35.0, 5: 37.1, 6: 39.2, 7: 42.0, 8: 44.8, 9: 47.6, 10: 50.4, 11: 53.31, 12: 57.12, 13: 60.93, 14: 64.74, 15: 68.54}
CHARGED_MV_DICT = {1: 174.08, 2: 187.14, 3: 200.19, 4: 217.6, 5: 230.66, 6: 243.71, 7: 261.12, 8: 278.53, 9: 295.94, 10: 313.34, 11: 331.45, 12: 355.12, 13: 378.8, 14: 402.47, 15: 426.15}
JADE_STAR_MV_DICT = {1: 49.6, 2: 53.32, 3: 57.04, 4: 62.0, 5: 65.72, 6: 69.44, 7: 74.4, 8: 79.36, 9: 84.32, 10: 89.28, 11: 94.44, 12: 101.18, 13: 107.93, 14: 114.68, 15: 121.42}
JADE_SCREEN_MV_DICT = {1: 230.4, 2: 247.68, 3: 264.96, 4: 288.0, 5: 305.28, 6: 322.56, 7: 345.6, 8: 368.64, 9: 391.68, 10: 414.72, 11: 437.76, 12: 460.8, 13: 489.6, 14: 518.4, 15: 547.2}
STARSHATTER_MV_DICT = {1: 86.96, 2: 93.48, 3: 100.0, 4: 108.7, 5: 115.22, 6: 121.74, 7: 130.44, 8: 139.14, 9: 147.83, 10: 156.53, 11: 165.22, 12: 173.92, 13: 184.79, 14: 195.66, 15: 206.53}

from check_HiScore_math import debug_print

def calculate_combo_multiplier(talent_level=6, n_normal_charged_combos=8, atk_bonus=1.5, dmg_bonus=0.706, normal_bonus=0.0, charged_bonus=0.0, skill_bonus=0.0, ult_bonus=0.0, 
                               printing=False, combo_type="1_normal_point_blank", constellation=0, full_rotation_time=15, n_ult_gems_hit=12, 
                               normal_attack_bonus=0.0, enable_debug_print=False):
    '''Currently, Ningguang can throw our 2 normal and 2 charged in 3 seconds.
    
    This simulates 8 combos + skill + ult every 12 seconds. (In reality, some combo time will be lost to swaps and movement)
    
    Note: Every 12 seconds 8 combos can be thrown on November 12, 2020.
    
    Note: normal_bonus is damage bonus, normal_attack_bonus is an attack boost that applies only to normal attacks (eg. everlasting moonglow).
    '''
    normal_scaling = NORMAL_MV_DICT[talent_level] * 2.0
    charged_scaling = CHARGED_MV_DICT[talent_level]
    jade_star_scaling = JADE_STAR_MV_DICT[talent_level]
    
    if constellation > 2:
        jade_screen_scaling = JADE_SCREEN_MV_DICT[talent_level+3]
    else:
        jade_screen_scaling = JADE_SCREEN_MV_DICT[talent_level]
    
    if constellation > 4:
        starshatter_scaling = STARSHATTER_MV_DICT[talent_level+3] * float(n_ult_gems_hit)
    else:
        starshatter_scaling = STARSHATTER_MV_DICT[talent_level] * float(n_ult_gems_hit)
    
    if constellation > 1:
        jade_screen_scaling = jade_screen_scaling * 2
        
    normal_damage = (1.0 + atk_bonus + normal_attack_bonus) * (normal_scaling * (1 + dmg_bonus + normal_bonus))
    charged_damage = (1.0 + atk_bonus) * charged_scaling * (1 + dmg_bonus + charged_bonus)    
    jade_star_damage = (1.0 + atk_bonus) * jade_star_scaling * (1 + dmg_bonus + charged_bonus)
        
    if combo_type == "1_normal_point_blank":
        jade_star_damage = 2 * jade_star_damage
        # NOTE: At point blank normal attacking will give you 2 star jades. Kevsie's video: https://www.youtube.com/watch?v=8N3emPQZkjs&feature=youtu.be
    elif combo_type == "1_normal":
        pass # No multiplications needed.
    elif combo_type == "2_normal": # Note: In 12 seconds 4.8
        normal_damage = 2 * normal_damage
        jade_star_damage = 2 * jade_star_damage
    elif combo_type == "3_normal":
        normal_damage = 3 * normal_damage
        jade_star_damage = 3 * jade_star_damage
    else:
        raise ValueError("Unrecognized combo_type: " + combo_type)
        
    combo_damage = normal_damage + charged_damage + jade_star_damage
        
    skill_damage = (1.0 + atk_bonus) * jade_screen_scaling * (1 + dmg_bonus + skill_bonus)
    ult_damage = (1.0 + atk_bonus) * starshatter_scaling  * (1 + dmg_bonus + ult_bonus)
    
    if constellation == 6:
        ult_damage += (1.0 + atk_bonus) * jade_star_scaling * (1 + dmg_bonus + charged_bonus) * 7
    
    effective_combo_multiplier = (combo_damage * n_normal_charged_combos + skill_damage + ult_damage) / full_rotation_time
    
    if printing:
        print effective_combo_multiplier   
        
    if enable_debug_print:
        debug_print(n_normal_charged_combos, normal_scaling, charged_scaling, jade_screen_scaling, starshatter_scaling, jade_star_scaling, atk_bonus, dmg_bonus, 
                    normal_bonus, charged_bonus, skill_bonus, ult_bonus)
        
    return effective_combo_multiplier
    
def print_result(multiplier, base_multiplier):
    print multiplier, "(" + str((multiplier / base_multiplier) * 100.0 - 100) + "% increase over base)"
    
def main():
    baseline_attack_bonus = 2.36 # Kevsie's test attack bonus
    
    #baseline_attack_bonus = 1.5
    
    print "No set bonus:"
    multiplier = calculate_combo_multiplier(atk_bonus=baseline_attack_bonus)
    base_multiplier = multiplier
    print_result(multiplier, base_multiplier)
    
    print "\n2pc Gladiator + 2pc Petra"
    multiplier = calculate_combo_multiplier(atk_bonus=baseline_attack_bonus+0.18, dmg_bonus=0.706+0.15)
    print_result(multiplier, base_multiplier)
    
    print "\n2pc Petra + 2pc Noblesse Oblige"
    multiplier = calculate_combo_multiplier(atk_bonus=baseline_attack_bonus, dmg_bonus=0.706+0.15, ult_bonus=0.20)
    print_result(multiplier, base_multiplier)
    
    print "\n4pc Retracing Bolide"
    multiplier = calculate_combo_multiplier(atk_bonus=baseline_attack_bonus, normal_bonus=0.4, charged_bonus=0.4)
    print_result(multiplier, base_multiplier)
    
    print "\n4pc Retracing Bolide (30% downtime)"
    multiplier = calculate_combo_multiplier(atk_bonus=baseline_attack_bonus, normal_bonus=0.4 * 0.7, charged_bonus=0.4 * 0.7)
    print_result(multiplier, base_multiplier)   
    
    print "\n4pc Wanderer's Troupe"
    multiplier = calculate_combo_multiplier(atk_bonus=baseline_attack_bonus, charged_bonus=0.35)
    print_result(multiplier, base_multiplier)
    
    print "\n3 hits: No set bonus:"
    multiplier = calculate_combo_multiplier(n_normal_charged_combos=3, atk_bonus=baseline_attack_bonus)    
    base_multiplier = multiplier
    print_result(multiplier, base_multiplier)
    
    print "\n3 hits: 2pc Gladiator + 2pc Petra"
    multiplier = calculate_combo_multiplier(n_normal_charged_combos=3, atk_bonus=baseline_attack_bonus+0.18, dmg_bonus=0.706+0.15)
    print_result(multiplier, base_multiplier)
    
    print "\n3 hits: 2pc Petra + 2pc Noblesse Oblige"
    multiplier = calculate_combo_multiplier(n_normal_charged_combos=3, atk_bonus=baseline_attack_bonus, dmg_bonus=0.706+0.15, ult_bonus=0.20)
    print_result(multiplier, base_multiplier)
    
    print "\n3 hits: 4pc Retracing Bolide"
    multiplier = calculate_combo_multiplier(n_normal_charged_combos=3, atk_bonus=baseline_attack_bonus, normal_bonus=0.4, charged_bonus=0.4)
    print_result(multiplier, base_multiplier)
    
    print "\n3 hits: 4pc Retracing Bolide (30% downtime)"
    multiplier = calculate_combo_multiplier(n_normal_charged_combos=3, atk_bonus=baseline_attack_bonus, normal_bonus=0.4 * 0.7, charged_bonus=0.4 * 0.7)
    print_result(multiplier, base_multiplier)
    
def compare_to_HiScore_math():
    # Compare to HiScore's math.
    print "\n4pc Retracing Bolide"
    multiplier = calculate_combo_multiplier(combo_type="1_normal", atk_bonus=1.6989010989, dmg_bonus=0.5, normal_bonus=0.4, charged_bonus=0.4, enable_debug_print=True)
    
    print 637 * multiplier * 12.0
    
if __name__ == "__main__":
    main()
    
    #compare_to_HiScore_math()
    
