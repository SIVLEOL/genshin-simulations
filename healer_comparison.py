
# Calculate at talent level 10

def qiqi():
    attack = 1908
    attack_bonus = 2.0824
    crit_rate = 55.2
    crit_damage = 128.7
    passive_healing_bonus = 22.2 # Note: Qiqi has an additional 0.20 bonus for chars that trigger elemental reactions for 8 seconds.

    print "Qiqi skill (passively):", 4 * (attack * 1.2528 + 991.28) / 30
    print "Qiqi skill (per hit):", (attack * 0.1901 + 148.3) * 36.75 / 30
    print "Qiqi passive and ult:", (attack * 1.62 + 1270) * 36.75 / 30  # 20 second cooldown, every 0.5 seconds, can be up 21/30 seconds

    # 7 Qiqi full combos in slightly more than 20 seconds when hitting a tree, 5 * 7 = 35 hits in 20 seconds, or 52.5 hits per 30 seconds
    
def barbara():
    health = 24176
    health_bonus = 2.19759291099061
    health_90 = 31294.8 # 9787 * (1 + 2.19759291099061)
    
    print "Barbara skill (passively):", (health_90 * 0.072 + 847.47) * 5 / 27.2 # Can heal 5 times.
    print "Barbara skill (per hit):", (health_90 * 0.0135 + 158.9) * 39 / 20 / 1.813 # Only up half the time.
    print "Barbara ult:", (health_90 * 0.3344 + 4024) / 20
    
    # 9 combos * 4 + 3 hits in 20 seconds. 39 hits per 20 seconds.
    # 27.2 / 15 = 1.813 (Barbara constellation reduces CD by 15%)
    
def jean():
    passive_healing_bonus = 22.2
    
    attack = 1908 # From Qiqi (jean has less base attack actually)
    
    print "Jean normal attacks:", (attack * 0.15) * 5 /3
    print "Jean ult (burst):", (attack * 4.5216 + 3388) / 20
    print "Jean ult (sustained):", 9 * (attack * 0.4522 + 338) / 20
    
    # Takes about 3 seconds to do 5 hits.
    
def kokomi():
    passive_healing_bonus = 25
    
    health_90 = 43074.77 # 13471 * (1 + 2.19759291099061)
    
    print "Kokomi skill:", (health_90 * 0.0792 + 932.22)* 7 / 20
    print "Kokomi ult:", (health_90 * 0.0145 + 169.49) * 19.5 / 18 # Don't know KOkomi's attack speed, so used Barbaras lul
    
def sayu():
    
    attack = 1908 # From qiqi (Sayu has way less base attack actually)
    
    print "Sayu ult burst (Qiqi stats):", (attack * 1.6589 + 1270) / 20
    print "Sayu ult sustained (Qiqi stats):", 7 * (attack * 1.4377 + 1100) / 20

def diona():
    
    health = 27176
    
    print "Diona ult:", (health * 0.096 + 1129) * 12 / 20 / 2 # Roughly 2 seconds per tick.

def main():
    qiqi()
    barbara()
    jean()
    kokomi()
    sayu()
    diona()
    
if __name__ == "__main__":
    main()