import sys
from Ningguang_weapon_comparison import simulate
from collections import OrderedDict
from cStringIO import StringIO

def compare(bennett_buff=False, n_normal_charged_combos=8, detailed_print=False):
    # Slightly modified default kwargs from GUI.
    ap_glad_kwargs = {'lost_prayer_passive_stacks': 1.0, 'frostbearer_refinement': 1.0, 'blackcliff_refinement': 1.0, 'twin_nephrite_passive_active': False, 'print_parameters': False, 
                      'skyward_atlas_passive_mode': 'average', 'lost_prayer_refinement': 1.0, 'dodoco_tales_charged_passive_enabled': 'True', 'constellation': 6.0, 
                      'artifact_normal_bonus': 0, 'eye_of_perception_refinement': 1.0, 'primordial_jade_regalia_refinement': 1.0, 'favious_codex_refinement': 1.0, 
                      'blackcliff_stacks': 0.0, 'royal_grimoire_refinement': 1.0, 'cryo_on_enemy': False, 'artifact_substat_attack_percentage': 0.14599999999999996, 
                      'mappa_mare_refinement': 1.0, 'artifact_attack_percentage': 0.792, 'mappa_mare_stacks': 2.0, 'mode': 'optimized', 'geo_damage_percentage': 0.856, 
                      'memory_of_dust_refinement': 1.0, 'twin_nephrite_refinement': 5.0, 'wine_and_song_refinement': 1.0, 'wine_and_song_passive_active': True, 'artifact_ult_bonus': 0, 
                      'artifact_flat_attack': 373.0, 'eye_of_perception_bounces': 1.0, 'widsith_buff_mode': 'average', 'dodoco_tales_refinement': 1.0, 'artifact_crit_rate': 0.703, 
                      'dodoco_tales_attack_passive_enabled': 'True', 'n_normal_charged_combos': 6.0, 'talent_level': 10.0, 'artifact_crit_damage': 1.1909999999999998, 
                      'artifact_substat_flat_attack': 62.0, 'combo_type': '1_normal', 'memory_of_dust_shield_active': True, 'artifact_charged_bonus': 0, 'widsith_refinement': 1.0, 
                      'royal_grimoire_n_simulations': 1000.0, 'memory_of_dust_passive_stacks': 5.0, 'skyward_atlas_refinement': 1.0, 'solar_pearl_refinement': 1.0, 
                      'full_rotation_time': 12.0}

    bolide_kwargs = {'lost_prayer_passive_stacks': 1.0, 'frostbearer_refinement': 1.0, 'blackcliff_refinement': 1.0, 'twin_nephrite_passive_active': False, 'print_parameters': False, 
                     'skyward_atlas_passive_mode': 'average', 'lost_prayer_refinement': 1.0, 'dodoco_tales_charged_passive_enabled': 'True', 'constellation': 6.0, 
                     'artifact_normal_bonus': 0.4, 'eye_of_perception_refinement': 1.0, 'primordial_jade_regalia_refinement': 1.0, 'favious_codex_refinement': 1.0, 
                     'blackcliff_stacks': 0.0, 'royal_grimoire_refinement': 1.0, 'cryo_on_enemy': False, 'artifact_substat_attack_percentage': 0.14599999999999996, 
                     'mappa_mare_refinement': 1.0, 'artifact_attack_percentage': 0.612, 'mappa_mare_stacks': 2.0, 'mode': 'optimized', 'geo_damage_percentage': 0.706, 
                     'memory_of_dust_refinement': 1.0, 'twin_nephrite_refinement': 5.0, 'wine_and_song_refinement': 1.0, 'wine_and_song_passive_active': True, 'artifact_ult_bonus': 0, 
                     'artifact_flat_attack': 373.0, 'eye_of_perception_bounces': 1.0, 'widsith_buff_mode': 'average', 'dodoco_tales_refinement': 1.0, 'artifact_crit_rate': 0.703, 
                     'dodoco_tales_attack_passive_enabled': 'True', 'n_normal_charged_combos': 6.0, 'talent_level': 10.0, 'artifact_crit_damage': 1.1909999999999998, 
                     'artifact_substat_flat_attack': 62.0, 'combo_type': '1_normal', 'memory_of_dust_shield_active': True, 'artifact_charged_bonus': 0.4, 'widsith_refinement': 1.0, 
                     'royal_grimoire_n_simulations': 1000.0, 'memory_of_dust_passive_stacks': 5.0, 'skyward_atlas_refinement': 1.0, 'solar_pearl_refinement': 1.0, 
                     'full_rotation_time': 12.0}


    old_stdout = sys.stdout

    sys.stdout = stdout_redirect = StringIO()

    constellation_damage_score_dict = OrderedDict()

    # Get weapon names.
    simulate(**ap_glad_kwargs)
    weapon_names = [string.split(":")[0] for string in stdout_redirect.getvalue().split("\n")[::3] if string]

    for kwargs in [ap_glad_kwargs, bolide_kwargs]:
        if bennett_buff:
            kwargs["artifact_flat_attack"] = 1334.88
            
        kwargs["n_normal_charged_combos"] = n_normal_charged_combos

    for weapon_index, weapon_name in enumerate(weapon_names):
        constellation_damage_score_dict[weapon_name] = OrderedDict()
        
        for constellation_level in reversed(xrange(0,7)):
            for artifact_set, kwargs in [("AP+Glad", ap_glad_kwargs), ("Bolide", bolide_kwargs)]:
                kwargs["constellation"] = float(constellation_level)

                if constellation_level not in constellation_damage_score_dict[weapon_name]:
                    constellation_damage_score_dict[weapon_name][constellation_level] = OrderedDict()
                
                constellation_damage_score_dict[weapon_name][constellation_level][artifact_set] = str(simulate(**kwargs)[weapon_index]).split()[0]
        
    sys.stdout = old_stdout
    
    print "Percentage that AP+Glad is stronger than Bolide ("+str(int(n_normal_charged_combos))+" one normal combos) (1 stack LP) (optimized stats):"
    
    for weapon_name in constellation_damage_score_dict:
        print "\n", weapon_name
        for constellation_level in constellation_damage_score_dict[weapon_name]:
            print "C" + str(int(constellation_level)), 
            
            if detailed_print:
                for artifact_set in constellation_damage_score_dict[weapon_name][constellation_level]:
                    print artifact_set, constellation_damage_score_dict[weapon_name][constellation_level][artifact_set]
                
            print "%.2f" % ((float(constellation_damage_score_dict[weapon_name][constellation_level]["AP+Glad"]) /\
                             float(constellation_damage_score_dict[weapon_name][constellation_level]["Bolide"]) - 1.0) * 100.0) + "%"
        
if __name__ == "__main__":
    compare(bennett_buff=True, n_normal_charged_combos=4)