# Note: Not accounting for gravestone second passive.

import math

from artifact_information import FEATHER_ATTACK, ARTIFACT_ATTACK_PERCENTAGE, ARTIFACT_ELEMENTAL_DAMAGE_BONUS, ARTIFACT_CRIT_RATE, ARTIFACT_DEFENCE_PERCENTAGE
from artifact_information import get_substat
from utils import calculate_crit_multiplier

NOELLE_BASE_ATTACK_90 = 191
NOELLE_BASE_defence_90 = 799
NOELLE_DEFENCE_PERCENTAGE_90 = 0.30
WHITEBLIND_ATTACK_90 = 510
WHITEBLIND_defence_PERCENTAGE_90 = 0.517
WHITEBLIND_REFINE_PERCENTAGE_DICT = {0:0.0, 1:0.24, 2:0.30, 3:0.36, 4:0.42, 5:0.48}
GRAVESTONE_ATTACK_90 = 608
GRAVESTONE_ATTACK_PERCENTAGE_90 = 0.20 + 0.496

def simulate(skill_level=6, substat_crit_rate=0.25, substat_crit_damage=0.50+0.50, substat_attack_percentage=0.099, substat_defence_percentage=0.62, weapon="whiteblind", refine_level=1):
    average_attack_multiplier_dict = {6:1.28, 8:1.5025, 10:1.74}
    defence_multiplier_dict = {6:1.06, 8:1.14, 10:1.22}
    
    if weapon == "whiteblind":
        base_attack = NOELLE_BASE_ATTACK_90 + WHITEBLIND_ATTACK_90
        
        attack_from_defence = NOELLE_BASE_defence_90 * \
            (1 + ARTIFACT_DEFENCE_PERCENTAGE + WHITEBLIND_defence_PERCENTAGE_90 + WHITEBLIND_REFINE_PERCENTAGE_DICT[refine_level] + substat_defence_percentage) * \
            defence_multiplier_dict[skill_level]
        
        weapon_attack_percentage = WHITEBLIND_REFINE_PERCENTAGE_DICT[refine_level]
    elif weapon == "wolf's gravestone":
        base_attack = NOELLE_BASE_ATTACK_90 + GRAVESTONE_ATTACK_90
        
        attack_from_defence = NOELLE_BASE_defence_90 * \
            (1 + ARTIFACT_DEFENCE_PERCENTAGE + substat_defence_percentage) * \
            defence_multiplier_dict[skill_level]
        
        weapon_attack_percentage = GRAVESTONE_ATTACK_PERCENTAGE_90
    else:
        raise ValueError("Invalid weapon: " + weapon)        
    
    damage = (base_attack * (1 + weapon_attack_percentage + substat_attack_percentage) + FEATHER_ATTACK + attack_from_defence) * \
        average_attack_multiplier_dict[skill_level] * \
        (1 + ARTIFACT_ELEMENTAL_DAMAGE_BONUS) * calculate_crit_multiplier(substat_crit_rate+ARTIFACT_CRIT_RATE, substat_crit_damage)
        
    print weapon, damage
    
    return damage
    
def defence_step_up_comparison():
    for whiteblind_refine_level in xrange(1,6): 
        for skill_level in [6, 8, 10]:
            for defence_substat_rolls in [5, 10, 15, 20, 25, 30]:
                substat_defence_percentage = get_substat(defence_substat_rolls, "defence percentage")
                
                print "\n---refine_level:", whiteblind_refine_level, "skill_level:", skill_level, "substat_defence_percentage:", substat_defence_percentage, "---"
                simulate(skill_level, substat_defence_percentage=substat_defence_percentage, weapon="whiteblind", refine_level=whiteblind_refine_level)
                simulate(skill_level, substat_defence_percentage=substat_defence_percentage, weapon="wolf's gravestone") 
     
def budget_power(power_budget):
    max_crit = math.floor(power_budget / 2.0)
    
    crit_n_list = range(0, int(max_crit), 2)
    
    stats_list = []
    
    for crit_n in crit_n_list:
        crit_rate = get_substat(crit_n, "crit rate")
        crit_damage = get_substat(crit_n, "crit damage")
        defence_percentage = get_substat(power_budget - (crit_n * 2), "defence percentage")
    
        stats_list.append((crit_rate, crit_damage, defence_percentage))
        
    return stats_list
            
def power_budget_comparison():
    power_budget = 25 # The number of substats that roll into crit rate, crit damage or defence. Maximum is 40.
    
    variations = budget_power(power_budget)
    
    for whiteblind_refine_level in xrange(0,6):
        max_whiteblind = {"damage":0, "values":[]}
        max_gravestone = {"damage":0, "values":[]}
        
        for skill_level in [6, 8, 10]:
            for substat_crit_rate, substat_crit_damage, substat_defence_percentage in variations:
                print "\n---refine_level:", whiteblind_refine_level, "skill_level:", skill_level, "substat_crit_rate:", substat_crit_rate, \
                      "substat_crit_damage:", substat_crit_damage, "substat_defence_percentage:", substat_defence_percentage, "---"
                w_damage = simulate(skill_level, substat_crit_rate=substat_crit_rate, substat_crit_damage=substat_crit_damage, 
                                    substat_defence_percentage=substat_defence_percentage, weapon="whiteblind", refine_level=whiteblind_refine_level)
                g_damage = simulate(skill_level, substat_crit_rate=substat_crit_rate, substat_crit_damage=substat_crit_damage, 
                                    substat_defence_percentage=substat_defence_percentage, weapon="wolf's gravestone") 
                
                if w_damage > max_whiteblind["damage"]:
                    max_whiteblind["damage"] = w_damage
                    max_whiteblind["values"] = (substat_crit_rate, substat_crit_damage, substat_defence_percentage)
                    
                if g_damage > max_gravestone["damage"]:
                    max_gravestone["damage"] = g_damage
                    max_gravestone["values"] = (substat_crit_rate, substat_crit_damage, substat_defence_percentage)
                    
        print "max_whiteblind", max_whiteblind
        print "max_gravestone:", max_gravestone
            
if __name__ == "__main__":
    power_budget_comparison()