import sys, os
import tkinter as tk

def calculate_crit_multiplier(crit_rate_bonus, crit_damage_bonus):
    '''Note: Considering the 5% crit rate and 50% base crit damage from base stats as part of bonus, since it's easier to get the number that way.'''
    if crit_rate_bonus > 1.0:
        crit_rate_bonus = 1.0
    
    return 1 + (crit_rate_bonus * crit_damage_bonus)

def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

def setup_scroll_section(parent, canvas_background_color=None, frame_background_color=None, vertical_scroll=True, horizontal_scroll=False, canvas_kwargs=None, **kwargs):
    '''Some allowed kwargs: height, width'''
    if canvas_kwargs is None:
        canvas_kwargs = dict()
    
    canvas = tk.Canvas(parent, background=canvas_background_color, highlightthickness=0, **canvas_kwargs)
    frame = tk.Frame(canvas, background=frame_background_color)
    main_window = canvas.create_window((0,0), window=frame, anchor='nw', **kwargs)
    
    def on_configure(event):
        # update scrollregion after starting 'mainloop'
        # when all widgets are in canvas
        canvas.configure(scrollregion=canvas.bbox(main_window))       
        
        # Canvas should expand to same size as frame if there will be no scrollbar for an axis.
        if not horizontal_scroll:
            canvas.configure(width=frame.winfo_reqwidth())
            
        if not vertical_scroll:
            canvas.configure(width=frame.winfo_reqheight())          
        
        #Scroll to the top-left of the window by default
        canvas.yview_moveto(0)
        canvas.xview_moveto(0)
    
    # --- Create scrollbars ---
    if vertical_scroll:
        vertical_scrollbar = tk.Scrollbar(parent, command=canvas.yview)
        vertical_scrollbar.pack(side=tk.RIGHT, fill='y')
        canvas.configure(yscrollcommand = vertical_scrollbar.set)
    
    if horizontal_scroll:
        horizontal_scrollbar = tk.Scrollbar(parent, orient=tk.HORIZONTAL, command=canvas.xview)
        horizontal_scrollbar.pack(side=tk.BOTTOM, fill='x')
        canvas.configure(xscrollcommand = horizontal_scrollbar.set)            
    
    canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True) 
    
    # update scrollregion after starting 'mainloop'
    # when all widgets are in canvas
    canvas.bind('<Configure>', on_configure)
    
    return frame, canvas