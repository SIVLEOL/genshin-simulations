# Note: Everything is assuming C6 Fischl
NORMAL_LEVEL_6 = 64.1 + 68.0 + 84.5 + 83.9 + 105
SKILL_LEVEL_6 = 162
OZ_LEVEL_6 = 124 + 200 # 200 from constellation.
ULT_LEVEL_6 = 291 + 222 # 222 from constellation.

SECONDS_PER_COMBO = 3.0

FISCHL_BASE_ATTACK_90 = 244

from utils import calculate_crit_multiplier

def calculate_combo_multiplier(atk_bonus=1.5, physical_dmg_bonus=0.583, electro_dmg_bonus=0.0, normal_bonus=0.80, printing=False):
    # Assume 96% up time | Expected mechanic: (ult -> 12 -> skill -> 12 -> ult -> 12 -> 1(dead) -> skill -> 12 -> ult -> 12 -> 1(dead) -> skill -> 12 -> )
    # 74 seconds in total above.
    oz_dps = ((ULT_LEVEL_6 * 3 + SKILL_LEVEL_6 * 3 + (OZ_LEVEL_6 * 74) * 0.96) / 74) * (1 + atk_bonus) * (1 + electro_dmg_bonus) 
    
    normal_dps = NORMAL_LEVEL_6 * (1 + atk_bonus) * (1 + physical_dmg_bonus + normal_bonus) / SECONDS_PER_COMBO
    C6_dps = 30 * (1 + atk_bonus) * (1 + electro_dmg_bonus) / SECONDS_PER_COMBO

    effective_combo_multiplier = normal_dps + oz_dps + C6_dps

    if printing:
        print effective_combo_multiplier

    return effective_combo_multiplier

def compare_goblet():
    print "Electro goblet:", calculate_combo_multiplier(physical_dmg_bonus=0.583, electro_dmg_bonus=0.0)
    
    print "Physical goblet:", calculate_combo_multiplier(physical_dmg_bonus=0.0, electro_dmg_bonus=0.466)
    
def get_rust_stats(refinement=5):
    base_attack = 510
    
    attack_bonus = 0.413
    normal_bonus = 0.40 + 0.10 * (refinement-1)
    
    return base_attack, attack_bonus, normal_bonus
    
def simulate(artifact_attack_percentage=1.344, artifact_crit_rate=0.453, artifact_crit_damage=0.50+0.387, electro_dmg_bonus=0.0, physical_dmg_bonus=0.833):
    # --- R5 Rust ---
    
    base_attack, attack_bonus, normal_bonus = get_rust_stats()
    base_attack += FISCHL_BASE_ATTACK_90
    
    print "R5 Rust:", calculate_damage_score(base_attack, artifact_attack_percentage, artifact_crit_rate, artifact_crit_damage, attack_bonus=attack_bonus,
                                             physical_dmg_bonus=physical_dmg_bonus, normal_bonus=normal_bonus)
    
def calculate_damage_score(base_attack, artifact_attack_percentage, artifact_crit_rate, artifact_crit_damage, 
                           attack_bonus=0.0, crit_rate_bonus=0.0, crit_damage_bonus=0.0, weapon_passive_multiplier=0.0, **kwargs):
    damage_score = base_attack * (calculate_combo_multiplier(atk_bonus=artifact_attack_percentage + attack_bonus, **kwargs) + weapon_passive_multiplier) / 100.0 \
        * calculate_crit_multiplier(artifact_crit_rate+crit_rate_bonus, artifact_crit_damage+crit_damage_bonus)
    
    return damage_score        
    
def main():
    print "Goblet comparison:"
    compare_goblet()
    
    print "\nPhysical goblet:"
    simulate(electro_dmg_bonus=0.0, physical_dmg_bonus=0.833)
    
    print "\nElectro goblet:"
    simulate(electro_dmg_bonus=0.466, physical_dmg_bonus=0.25)
    
if __name__ == "__main__":
    main()
    
    