from stat_balancing import balance_crit

def balance_stats(base_attack, attack, crit_rate, crit_damage, energy_recharge, mode="Xiangling"):
    # Assumptions: Elemental goblet is always worth bringing.
    # Note: It's possible for this to give an impossible comibnation, need to manually check.
    # TODO: Apply DRY to substat balancing values. 
    
    attack_percentage = attack / float(base_attack)
    
    minimum_crit_rate = 0.5
    minimum_crit_damage = 0.50     
    minimum_energy_recharge = 0.20
    
    if mode == "Xiangling":
        passive_attack = 225
        minimum_energy_recharge += 0.306 # Favonius lance
        energy_recharge_elemental_multiplier = 0.3
    elif mode == "Mona":
        passive_attack = 0
        minimum_crit_damage += 0.551
        minimum_energy_recharge += 0.320 # Mona passive
        energy_recharge_elemental_multiplier = 0.5
    else:
        raise ValueError("Error: Unexpected mode: " + str(mode))
    
    minimum_attack_percentage = (base_attack + 311.0 + passive_attack) / base_attack   
    
    # ..... estimate the ideal attack for crit by binary search style trial and error because I can't math.
    
    # Janky stat balancing based on main artifact stats (not true substat averages)
    stats_pool = attack_percentage / 0.0466 + crit_rate / 0.0311 + crit_damage / 0.0622 + energy_recharge / 0.0518 + 5 # 5 is from elemental damage
    
    max_attack = minimum_attack_percentage + 0.466 + 0.466 + ((16.5) * 5 / base_attack) * 2 + 0.04975 * 5 * 3 # True average substat attack used here.
    max_energy_recharge = minimum_energy_recharge + 0.518 + 4 * 0.055 # True average substat energy recharge used here
    
    max_attack_pool = int(round(max_attack / 0.0466))
    min_attack_pool = int(round(minimum_attack_percentage / 0.0466))
    min_crit_pool = int(round((minimum_crit_rate + minimum_crit_damage / 2) / 0.0311))
    max_crit_pool = min_crit_pool + int(7 * 4 + 7 + 10) # rate and damage on 4, crit rate hat, damage substat maxed
    max_energy_recharge_pool = int(round(max_energy_recharge / 0.0518))
    min_energy_recharge_pool = int(round(minimum_energy_recharge / 0.0518))
    
    max_value = 0
    max_parameters = None
    max_pool = None
    
    for elemental_points in [5, 0]:
        for attack_points in xrange(min_attack_pool, max_attack_pool+1):
            for crit_points in xrange(min_crit_pool, max_crit_pool+1):
                for energy_recharge_points in xrange(min_energy_recharge_pool, max_energy_recharge_pool+1):
                    if elemental_points + attack_points + crit_points + energy_recharge_points > stats_pool:
                        continue
                    
                    if elemental_points:
                        elemental_damage = 0.466
                    else:
                        elemental_damage = 0
                    
                    b_attack = attack_points * 0.0466
                    b_crit_rate, b_crit_damage = balance_crit(crit_points * 0.0311, 0)
                    b_energy_recharge = energy_recharge_points * 0.0518
                    
                    value = b_attack * (1 + b_crit_rate * b_crit_damage) * (1 + elemental_damage + b_energy_recharge * energy_recharge_elemental_multiplier)
                    
                    if value > max_value:
                        if value in [5.740555605946666, 6.107350697087701]:
                            pass
                        
                        max_value = value
                        max_parameters = (elemental_damage, b_attack, b_crit_rate, b_crit_damage, b_energy_recharge)
                        max_pool = (int(elemental_damage > 0), attack_points, crit_points, energy_recharge_points)
        
    return max_value, zip(["ele_damage", "atk", "c_rate", "c_damage", "er"], max_parameters), \
           max_pool, [1, max_attack_pool, max_crit_pool, max_energy_recharge_pool] 
    
def simulate_Xiangling(base_attack=695, attack=1841, crit_rate=0.26, crit_damage=0.904, energy_recharge=0.707):
    for element in balance_stats(base_attack, attack, crit_rate, crit_damage, energy_recharge, mode="Xiangling"):
        print element
    
def simulate_Mona(base_attack=695, attack=1841, crit_rate=0.26, crit_damage=0.904, energy_recharge=0.707): # Using same values as Xiangling for now.
    for element in balance_stats(base_attack, attack, crit_rate, crit_damage, energy_recharge, mode="Mona"):
        print element
    
if __name__ == "__main__":
    # TODO: results coming out still too high for the given amount of input stats.
    
    simulate_Xiangling()
    simulate_Mona()
    
     