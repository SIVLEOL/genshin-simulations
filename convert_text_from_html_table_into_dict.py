
# Note: Use html property "user-select: text" to make the text selectable.

def convert(text):
    result_dict = {i+1:float(value.replace("%", "")) for i, value in enumerate(text.split("\t"))}
    print result_dict
    
if __name__ == "__main__":
    convert("86.96%	93.48%	100%	108.7%	115.22%	121.74%	130.44%	139.14%	147.83%	156.53%	165.22%	173.92%	184.79%	195.66%	206.53%")