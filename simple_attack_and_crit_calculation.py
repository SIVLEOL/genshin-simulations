
def calculate(attack, crit_rate, crit_damage, elemental_damage=0.0, aggravate_EM=None, speed=100):
    if aggravate_EM is not None:
        aggravate_base_damage_bonus = 1663.88
        attack += (aggravate_base_damage_bonus * 1.15 * (1.0+((5.0+aggravate_EM)/(aggravate_EM+1200.0)+0.0)))
    
    print str(attack) + " x (1 + "+str(crit_rate/100.0)+" x " + str(crit_damage/100.0) + ")", 
    
    if elemental_damage:
        print "x (1 + " + str(elemental_damage/100.0) + ")",
    
    if speed != 100:
        print "x (1 + " + str(speed/100.0) + ")",
    
    print "=", attack * (1 + crit_rate/100.0 * crit_damage/100.0) * (1 + elemental_damage/100.0) * (0 + speed/100.0)
 
def calculate_reaction(elemental_mastery, reaction_damage_bonus=0.0):
    EM_bonus = 16.0 * (elemental_mastery / (elemental_mastery + 2000.0)) * 100
    
    print EM_bonus + reaction_damage_bonus
    
if __name__ == "__main__":
    ## My Ning
    #calculate(2250+1000, 70.3, 119.1) # Used for a long time
    #calculate(2222+1000, 63.3, 155.7) # Old godroll flower
    #calculate(2131+1000, 57.1, 166.6) # Jean's hat
    #calculate(2131+1000, 61.4, 153.3) # Noelle's hat
    #calculate(2292+1000, 63.3, 160.3) # Eula's hat
    #calculate(2164+1000, 69.1, 140.9) # 14.8% crit rate glad feather
    #calculate(2286+1000, 70.3, 136.3) # Old Current (new near godroll flower)
    #calculate(2358, 70.3, 135.5) # Old current
    #calculate(2358, 78.5, 126.9) # Old current
    #calculate(2356, 70.3, 140.9) # Best hat, 5% ER
    #calculate(2376, 73.4, 139.4) # Current
    
    ##Albedo
    #calculate(2770, 61.8, 137.0) # Old current    
    #calculate(2696, 63.3, 150.3) # Current (best geo goblet)
    
    ## Noelle
    #calculate(1195+1751*1.3, 87.4, 176.7, 46.6) # Highest DPS glad
    #calculate(1297+1711*1.3, 84.3, 176.7, 46.6) # More ER glad
    #calculate(1269+1583*1.3, 72.2, 202.2, 46.6) # Even more ER glad
    #calculate(1248+(2057+799*0.24)*1.3, 55.9, 164.3, 46.6+(6*4)) # Husk 4 stacks STRONGEST
    #calculate(1248+(2041+799*0.24)*1.3, 47.7, 168.2, 46.6+(6*4)) # Current (nerfed to give Ning good goblet)
    #calculate(1389+(1999+799*0.24)*1.3, 48.1, 151.1, 46.6+(6*4))
    #calculate(1248+(2057+799*0.12)*1.3, 55.9, 164.3, 46.6+(6*3)) # Husk 3 stacks
    #calculate(1280+(2057+799*0.12)*1.3, 54.3, 151.1, 46.6+(6*3))
    #calculate(1248+(2057)*1.3, 55.9, 164.3, 46.6) # Husk 0 stacks
    
    ## Noelle
    #calculate(1195+(1751+464)*1.3, 87.4, 176.7, 46.6+35+50) # Note: Assumes talent level 9 and Gorou
    #calculate(1195-701*0.15+(1751+464+799*0.54)*1.3, 87.4, 176.7, 46.6+24+50) # Theoretical husk with same quality artifacts.
    #calculate(1195+(1751+464+799*0.28)*(1.3+0.4), 87.4-27.6, 176.7+88.2, 46.6+35) # Theoretical R1 redhorn thresher
    #calculate(1195+(1751+464+799*0.28)*(1.3+0.4), 87.4-27.6+35, 176.7+88.2-70, 46.6+35) # Theoretical R1 redhorn thresher rebalanced crit 
    
    ## My Eula
    ## New section with PF set and Rosaria crit rate and Noblesse effect
    #calculate((950*(2.69368421+0.18)), 60.2+30, 202.7, 108.3+30.0+50.0) # old1 (with constellation buffs)
    #calculate((950+133)*(2.69368421-0.496+0.16+0.2+0.18), 60.2+30, 202.7, 108.3+30.0+50.0+20.7) # (old1) Theoretical Pines (with constellation and weapon buffs)
    #calculate(950*(2.69368421-0.496+0.4), 60.2+33.08+7, 202.7+46, 108.3+30.0+50.0) # (old1) Theoretical dehya claymore (with constellation and weapon buffs, rebalance crit)
    calculate((2379+950*(0.18+0.2)), 59.1+30, 229.1, 83.3+25+30.0+50.0) # Current
    calculate((2085+950*(0.18+0.16+0.2)), 59.1+30, 229.1, 104.0+25.0+30.0+50.0)
    
    ## Old Eula section (pre-PF)
    #calculate(2743, 66.0+29, 173.1) # Old current (crit damage version)
    #calculate(2650+(950*0.18), 62.6+29, 176.2) # Balanced 
    #calculate(2606+(950*0.18), 58.3+29, 194.9) # Balanced pale flame setup
    #calculate(2604+(950*0.18), 63.7+29, 194.9) # Current | Low ER
    #calculate(2423+(950*0.18), 65.7+29, 194.9) # High ER
    #calculate(2604+(950*(0.18-0.15)), 63.7+29, 194.9) # Previous current Low ER
    #calculate(2560+(950*(0.18-0.15)), 60.2+29, 202.7) # Current | 118% ER | slightly weaker average damage
    
    #calculate(2785, 75, 150.6) # (crit rate version) (consistency and DPS but lower damage per screenshot)
    #calculate(2743+(950*0.05), 66.0, 173.1, 108.3) # If I refine WGS to R2
    #calculate(2560+190, 55.5, 171.5, 108.3) # Full pale flame ult
    #calculate(2743, 66.0, 173.1, 108.3)
    #calculate(1083+1094, 85.9, 175.4, 104.0)
    #calculate(2792+(0.20 * 950), 55.5, 176.2) # 4pc Pale Flame
    #calculate(2660+(0.20 * 950), 55.5, 178.5)
    #calculate(2653, 65.3, 182.4)
    #calculate(2683, 70.3, 177.0) # Even more damage focused.
    #calculate(2595, 69.5, 184.0) # Balanced (yoimiya nerf)]
    #calculate(2599, 76.5, 165.3) # ER buff.
    #calculate(2615, 59.4, 177.8) # Massive ER
    #calculate(2615, 64.1, 173.1) # Massive ER rebalance.
    #calculate(2560, 71.9, 183.2)
    #calculate(2779, 64.5, 166.1)
    
    
    #calculate((950*2.887368421), 66.0, 173.1, 108.3) # Reproduction of current
    #calculate((950+133)*(2.887368421-0.496-0.04), 66.0, 173.1, 108.3+20.7) # Theoretical Pines
    
    #calculate((950*2.887368421), 66.0, 173.1, 108.3+30.0+25.0) # Reproduction of current (with ascension buffs)
    #calculate((950+133)*(2.887368421-0.496-0.04), 66.0, 173.1, 108.3+20.7+30.0+25.0) # Theoretical Pines (with ascension buffs)
    
    #calculate((950*2.887368421+0.4), 66.0, 173.1, 108.3) # Reproduction of current (with weapon buff)
    #calculate((950+133)*(2.887368421-0.496-0.04+0.2)*1.12, 66.0, 173.1, 108.3+20.7) # Theoretical Pines (with weapon buff)
    
    #calculate((950*2.887368421+0.4), 66.0, 173.1, 108.3+30.0+25.0) # Reproduction of current (with ascension and weapon buffs)
    #calculate((950+133)*(2.887368421-0.496-0.04+0.2)*1.12, 66.0, 173.1, 108.3+20.7+30.0+25.0) # Theoretical Pines (with ascension and weapon buffs)
    
    #calculate(950*(2.887368421-0.496+0.2), 66.0+33.08, 173.1, 108.3+30.0+25.0) # Theoretical dehya claymore
    
    # Eula ult (combo not affected unless high refinement Pines)
    #calculate((950*2.887368421), 66.0, 173.1, 108.3+30.0+25.0) # Reproduction of current (with ascension and no WGS buff)
    #calculate((950*(2.887368421+0.2)), 66.0, 173.1, 108.3+30.0+25.0) # Reproduction of current (with ascension, no WGS buff, include ToTM buff)
    #calculate((950*(2.887368421+0.2+0.2)), 66.0, 173.1, 108.3+30.0+25.0) # Reproduction of current (with ascension, no WGS buff, include ToTM buff and Noblesse buff)
    #calculate((950*(2.887368421+0.4)), 66.0, 173.1, 108.3+30.0+25.0) # Reproduction of current (with ascension and WGS buffs)
    #calculate((950+133)*(2.887368421-0.496-0.04+0.2), 66.0, 173.1, 108.3+20.7+30.0+25.0) # Theoretical Pines (with ascension and weapon buffs)
    #calculate((950+133)*(2.887368421-0.496+0.12+0.4), 66.0, 173.1, 108.3+20.7+30.0+25.0) # Theoretical R5 Pines (with ascension and weapon buffs) 
    #calculate(2560, 76.7, 150.7, 108.3) # Theoretical modification to better generic flower
    #calculate(2516, 61.0, 180.1, 108.3) # Use godroll flower
    #calculate(2698, 66.0, 162.3, 108.3) # Theoretical if we had the right sets with ER to keep the better physical cup.
    #calculate(2698, 56.0, 182.3, 108.3) # Theoretical if we had the right sets (but crit damage) with ER to keep the better physical cup.
    #print "---"
    #calculate(2654, 55.5, 175.4, 108.3)
    #calculate(2654, 55.5, 182.4, 108.3)
    #calculate(2825, 61, 194.8, 83.3)
    #calculate(2698, 66, 162.2, 83.3)
    #calculate(2698, 59, 166.9, 108.3)
    #print "---"
    #calculate(2698, 66, 166.9, 108.3)
    #calculate(2743, 55.0, 173.1, 108.3)
    #calculate(2869, 62.9, 173.1, 83.3)
    #calculate(2787, 70.3, 154.4, 108.3)
    #calculate(2743, 56.3, 173.1, 83.3)
    #print "---"
    #calculate(2604, 66.0, 158.3, 108.3)
    #calculate(2516, 61.0, 180.1, 108.3)
    #calculate(2754, 71.2, 142.6, 108.3)
    #calculate(2665, 63.7, 152.9, 108.3)
    #calculate(2665, 71.2, 142.9, 108.3)
    #print "---"
    #calculate(2804, 59.4, 162.2, 108.3)
    #calculate(2762, 58.3+3, 162.2, 108.3)
    #calculate(2787, 56.3, 175.4, 108.3)
    
    ## Yoimiya
    #calculate(2269, 78.2, 152.6) 
    #calculate(2313, 79.4, 146.3) # Current
    
    ## Fischl (aggravate)
    #calculate(2018, 54.0, 115.3, aggravate_EM=385)
    #calculate(1887, 74.6, 133.9, aggravate_EM=294) # Current
    #calculate(1882, 68.0, 136.3, aggravate_EM=386)
    #calculate(1851, 72.3, 139.4, aggravate_EM=330)
    #calculate(1851, 72.3, 139.4, aggravate_EM=330-80)
    
    ## Keqing (aggravate)
    #calculate(1654, 90.9, 167.7, 61.6, aggravate_EM=119) # Old Thundering Fury
    #calculate(1654, 87.4, 180.3, 61.6, aggravate_EM=84) # Current Thundering Fury
    
    ##Ei
    #calculate(1454, 47.8, 92.0, 125.2)
    #calculate(1877, 47.8, 75.7, 89.2)
    
    ##Xiangling
    #calculate(1682+1000, 49.7+12, 154.2, 46.6+(206.8*0.25)+32) # Current, catch
    #calculate(2213+1000, 49.7, 220.3, 46.6+(160.8*0.25)) # Homa, loss of ER
    
    ## Beidou
    #calculate(2371, 43.1, 79.5, 58.6+(167*0.25)) # Old
    #calculate(2322, 39.6, 92.7, 58.6+(156*0.25))
    #calculate(2045, 45.8, 79.5, 58.6+(202*0.25))
    #calculate(2307, 65.7, 65.5, 58.6+(177.7*0.25)) # Current. More swirl damage.
    #calculate(2160, 61.8, 87.3, 58.6+(177.7*0.25)) # Most raw damage
    
    ## Zhongli # Note: Calculated with hydro resonance, assume talent level 10, focused on burst, numbers uncomparable with rest due to different units.
    #calculate((1463+709*0.2)*9+0.33*48956, 71.3, 127.7) # Current Deathmatch
    #calculate(1815*9+0.33*51626, 51.3, 165.9) # Current Homa
    
    ## Yelan # Note: Calculated with hydro resonance in Nahida hyperbloom (somehow this team has more HP)
    #calculate(39336, 88.4, 130.0+88, 61.6+20) # Higher damage | Not updated/accurate for Aqua Simulcra
    #calculate(38662, 98.5, 116.8+88, 61.6+20) # 116% ER, more damage | Not updated/accurate for Aqua Simulcra
    #calculate(36757, 98.5, 135.5+88, 61.6+20) # ER loss, highest damage | Not updated/accurate for Aqua Simulcra
    #calculate(40274, 95.7, 202.7, 61.6+20) # ER 130%, very slightly higher damage
    #calculate(40274, 93.0, 207.4, 61.6+20) # higher damage ER 134%
    #calculate(39600, 100.0, 188.7, 61.6+20) # lower consistent damage, ER 134%
    #calculate(37993, 95.7, 222.9, 61.6+20) # ER 125.9%
    #calculate(37993, 89.9, 236.1, 61.6+20) # ER 132.4%
    #calculate(37993, 93.0, 227.6, 61.6+20) # ER 129.8%
    #calculate(39645, 89.9, 232.2, 61.6+20) # (previous current), ER 125.9%
    #calculate(39645, 95.7, 219.0, 61.6+20) # ER 119.4%
    #calculate(38813, 89.9, 225.2, 61.6+20) # More stats, ER 144.7%
    #calculate(39935, 83.3, 210.5, 61.6+20) # ER 169.3%
    #calculate(38813, 98.8, 214.3, 61.6+20) # (previous current, less damage, balanced crit ratio), ER 133.0%
    #calculate(44922, 88.0, 237.7, 46.6+20) # (previous current) ER 128.5%
    #calculate(44414, 94.2, 236.1, 46.6+20) # Highest damage, ER 128.5%
    #calculate(43741, 97.7, 230.7, 46.6+20) # Current, slightly less damage with balanced crit ratio, ER 127.9%
    
    ## 3.6 Yelan set comparison
    #calculate(39645+(14450*0.40), 89.9, 232.2, 61.6+20) # HP/Hydro
    #calculate(39645-(14450*0.20), 89.9, 232.2, 61.6+20+15) # Theoretical new set, ER 125.9%
    #calculate(39645+(14450*0.60), 89.9, 232.2, 61.6+20-15) # Theoretical double HP set, ER 125.9%
     
    ## Xingqiu
    #calculate(1858, 60.2, 118.4, 81.6+20)
    #calculate(1779, 68.0, 120.7, 81.6+20) # Current
    #calculate(1722, 70.3, 126.9, 81.6+20)
    
    ##Mona
    #calculate(1911, 75.4, 93.5, 103.5) # 209% ER, okay damage.
    #calculate(1491, 61.4, 56.2, 102.6+(280.2*0.25))
    #calculate(1862, 49.3, 63.2, 98.1+(257.5*0.25))
    #calculate(1976, 68.4, 113.7, 101.3) # Low ER, high damage.
    #calculate(1831, 59, 56.2, 92.3+(228.4*0.25)) # 228% ER
    #calculate(1545, 55.2, 56.2, 107.9+(300.0*0.25)) # Current 306.7% ER
    #calculate(1452, 62.9, 57.8, 105.6+(295.1*0.25))
    
    ##Rosaria
    #calculate(1911+(694*0.2), 89.6, 135.5, 46.6+20)
    #calculate(1940+(694*0.2), 97.0, 123.1, 46.6+20) # Current
    
    ##Lisa
    #calculate(1140, 26.4, 92.7) # TTDS
    #calculate(1180, 12.8, 100.5)
    
    ## Raiden
    #calculate(1793, 54, 75.7, 85.4+(313.4*0.25))
    #calculate(1924, 46.2, 74.9, 85.1+(312.8*0.25)) # Current 
    
    ##Ganyu
    #calculate(2016, 37.3+20+20+15, 183.2)
    #calculate(2094, 30.7+20+20+15, 189.5)
    
    ## Geo Traveler
    #calculate(2126, 43.1+12, 123.8)
    
    ##Dendro traveler
    #calculate(1691, 60.2, 80.3) # Current
    #calculate(1596, 72.7, 73.3) 
    
    ## Klee
    #calculate(2309, 49.3, 121.5)
    #calculate(1847, 70.3, 158.7) # Widsith
    #calculate(1886, 63.7, 141.7)
    #calculate(2215, 63.7, 114.5)
    #calculate(2195, 63.7, 114.5) # Old current
    #calculate(1967, 60.2, 152.2) # Current (Widsith)
    
    ## Nahida
    #calculate(1140*1.445+(447+58)*2.89, 48.9+(((447+58)-200)*0.03), 165.7, 61.6+(447+58)*0.1) # Temporary +58 from ascension substat because not level 90 yet.
    
    #EM = 663
    #calculate(1252*1.445+(EM+58)*2.89, 39.2+(((EM+58)-200)*0.03), 170.4, 15.0+(EM+58)*0.1) # Temporary +58 from ascension substat because not level 90 yet.
    #calculate(1275*1.445+(EM+58)*2.89, 50.1+(((EM+58)-200)*0.03), 150.2, 15.0+(EM+58)*0.1) # Temporary +58 from ascension substat because not level 90 yet.
    
    ## Kuki
    #calculate_reaction(909+150)
    #calculate_reaction(860, 40) # 0 stacks
    #calculate_reaction(860, 40 + 25 * 4) # full stacks
    #calculate_reaction(860-50, 40 + 25 * 4) # full stacks
    
    
    # --- Star rail ---
    
    ## Hook (level 80)
    #calculate(((617+529)*(1+0.432+0.432))+352, 50, 100, 6*(308+71*3+110)*1.08+2*(432+110)*1.08*1.15) # x15 damage to simulate CR set advantage
    #calculate((((617+529)*(1+0.432))+352)*1.25, 50, 100, 6*(308+71*3+110)*1.08+2*(432+110)*1.08*1.15) # Speed instead