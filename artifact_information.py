FEATHER_ATTACK = 311
ARTIFACT_ATTACK_PERCENTAGE = 0.466
ARTIFACT_ELEMENTAL_DAMAGE_BONUS = 0.466
ARTIFACT_CRIT_RATE = 0.311
FLOWER_HEALTH = 4780
ARTIFACT_DEFENCE_PERCENTAGE = 0.583

def get_substat(n, substat="crit rate", value_type="average"):
    
    substat_range_dict = {"crit rate":(0.027, 0.039), "crit damage":(0.054,0.078), "attack percentage":(0.041, 0.058), "flat attack":(14,19), "defence percentage":(0.051,0.073), 
                          "flat DEFENCE":(16,23), "HP percentage":(0.041,0.058), "flat HP":(209,299), "elemental mastery":(16,23), "energy recharge":(0.045,0.065)}
    
    if substat not in substat_range_dict:
        raise ValueError("Substat '"+substat+"' does not exist. Substat must be in: "+str(substat_range_dict.keys()))
        
    if value_type == "average":
        return (substat_range_dict[substat][0] + substat_range_dict[substat][1]) / 2.0 * n
    elif value_type == "max":
        return substat_range_dict[substat][1] * n
    elif value_type == "min":
        return substat_range_dict[substat][0] * n
    else:
        raise ValueError("Invalid value_type: " + str(value_type))
    
    