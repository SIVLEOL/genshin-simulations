from Ningguang_weapon_comparison import simulate
import tkinter as tk
import inspect, ttk, os, sys, json
from tkinter import messagebox
from collections import OrderedDict
from cStringIO import StringIO
from utils import resource_path, setup_scroll_section

BACKGROUND_COLOR = "#f0f0f0"
SECTION_HEADER_FONT = ("Helvetica", 12, "bold")
BUFF_FRAME_COLOR = "#fdfffd"

VERSION = "1.8.2"

ICON_PATH = resource_path("ning_smug.ico")

class App(tk.Tk, object):
    
    boolean_options = ["Yes", "No"]
    
    dropdown_dict = {"talent_level":range(1,11), "combo_type":["2_normal", "1_normal", "1_normal_point_blank", "3_normal"], "constellation":range(7), 
                     "five_star_refinement":range(1,6), "four_star_refinement":range(1,6), "three_star_refinement":range(1,6),
                     "noblesse_2pc_set_bonus":boolean_options, "bolide_4pc_set_bonus":boolean_options,
                     "memory_of_dust_passive_stacks":range(0,6), "memory_of_dust_shield_active":boolean_options, 
                     "lost_prayer_passive_stacks":range(0,5), "blackcliff_stacks":range(0,4), "mappa_mare_stacks":range(0,3), 
                     "eye_of_perception_bounces":range(1,5), "cryo_on_enemy":boolean_options, "widsith_buff_mode":["average", "attack", "elemental damage", "none"], 
                     "twin_nephrite_passive_active":boolean_options, "wine_and_song_passive_active":boolean_options, "hakushin_ring_passive_active":boolean_options,
                     "bennett_talent_level":range(1,14), "dragonslayer_refinement":range(1,6), "skyward_atlas_passive_mode":["average", "on", "off"],
                     "kaguras_stacks":[range(0,4)], "full_rotation_time":[12, 15], "n_ult_gems_hit":[12,11,10,9,8,7,6,5]}
    
    bennet_talent_level_value_dict = {1: 0.56, 2: 0.602, 3: 0.644, 4: 0.7, 5: 0.742, 6: 0.784, 7: 0.84, 8: 0.896, 9: 0.952, 10: 1.008, 11: 1.064, 12: 1.12, 13: 1.19, 14: 1.26, 15: 1.33}

    
    rarity_refinement_dict = {"five_star_refinement":["memory_of_dust_refinement", "lost_prayer_refinement", "skyward_atlas_refinement", "everlasting_moonglow_refinement", 
                                                      "primordial_jade_regalia_refinement", "kaguras_refinement"],
                              "four_star_refinement":["solar_pearl_refinement", "blackcliff_refinement", "mappa_mare_refinement", "eye_of_perception_refinement", 
                                                      "frostbearer_refinement", "widsith_refinement", "favious_codex_refinement", "wine_and_song_refinement", "royal_grimoire_refinement",
                                                      "dodoco_tales_refinement", "hakushin_ring_refinement", "oathsworn_eye_refinement"],
                              "three_star_refinement":["twin_nephrite_refinement"]}
    
    help_text_dict = {} # Experimental functionality for adding help buttons to individual fields. Decided not to use it to avoid screen clutter for now. 
    
    def __init__(self, debug=False):
        super(App, self).__init__()
        
        self.debug = debug
        
        self.wm_title("SIVLEOL's Ningguang Weapon Comparison") 
        
        self.iconbitmap(ICON_PATH)
        
        self.width = min(self.winfo_screenwidth()-80, 1360)
        self.height = min(self.winfo_screenheight()-80, 898)
        
        self.geometry('%dx%d+%d+%d' % (self.width, self.height, 30, 30))
        
        self.auto_submit_var = tk.IntVar(self)
        self.auto_submit_var.set(0) # Setup for startup.
        
        tabs_frame = ttk.Notebook(self)
        tabs_frame.grid(row=0, column=1, sticky="NEWS", pady=(9,0), padx=(0,9))
        
        self.output_text = tk.Text(tabs_frame, height=1, state="disabled", width=55, relief="solid")
        self.output_text.grid(row=0, column=0, sticky="NEWS")
        
        tabs_frame.add(self.output_text, text="DPS")
        
        self.optimized_stats_output_text = tk.Text(tabs_frame, height=1, state="disabled", width=55, relief="solid")
        self.optimized_stats_output_text.grid(row=0, column=0, sticky="NEWS")        
        
        tabs_frame.add(self.optimized_stats_output_text, text="DPS (optimized stats)")
        
        self.create_menu()
        
        tk.Label(self, text="Version " + str(VERSION), font=("Helvetica", 8, "bold")).grid(column=0, sticky="w", padx=9, pady=2)
        
        # Load settings if available.
        self.load_settings()
        
        self.submit() # Show results of default values.
        
        self.help_window = None
        
    def create_menu(self):
        args = inspect.getargspec(simulate)
        
        # Format: [(field_1, value_1), ...]
        fields_and_defaults = zip(args.args[-len(args.defaults):],args.defaults)        
        
        outer_menu_frame = tk.Frame(self, borderwidth=1, relief="solid")
        outer_menu_frame.grid(row=0, column=0, sticky="news", ipadx=5, ipady=5, padx=9, pady=(9,0))
        outer_menu_frame.rowconfigure(0, weight=1)
        
        self.scrollable_menu_frame, self.canvas = setup_scroll_section(outer_menu_frame, canvas_kwargs={"height":self.height-62, "width":self.width-502}, horizontal_scroll=True)
        
        menu_frame = tk.Frame(self.scrollable_menu_frame)
        menu_frame.grid(row=0, column=0,  sticky="news")
        
        tk.Label(menu_frame, text="Stats", font=SECTION_HEADER_FONT).grid(row=0, column=0, sticky="w", padx=2, pady=(8,2))      
        
        self.field_var_dict = OrderedDict()
        current_row = 1
        current_column = 0
        in_weapon_section = False
        
        for field, default in fields_and_defaults:
            if "refinement" in field or field in ["artifact_normal_bonus", "artifact_charged_bonus", "artifact_skill_bonus", "artifact_ult_bonus", 
                                                  "weapon_refinement_dps_dict", "print_parameters", "mode", "artifact_substat_attack_percentage", "artifact_substat_flat_attack"]:
                continue
            
            custom_title = None
            
            if field == "artifact_crit_rate":
                field = "crit_rate"
                default = default * 100.0
            elif field == "artifact_crit_damage":
                field = "crit_damage"
                default = default * 100.0
            elif field in ["artifact_attack_percentage", "geo_damage_percentage"]:
                default = default * 100.0
                
                # Subtract default Archaic Petra/Gladiator set bonus.
                if field == "artifact_attack_percentage":
                    default = default - 18
                elif field == "geo_damage_percentage":
                    default = default - 15
            elif field == "full_rotation_time":
                custom_title = "Full rotation time (s)"
            elif field == "talent_level":
                custom_title = "Talent level (before constellations)"  
                
            self.add_menu_item(menu_frame, current_row, current_column, field, default, custom_title=custom_title)
            
            if field == "artifact_attack_percentage":
                self.attack_bonus_overlay_label = tk.Label(menu_frame, font=("Verdana", 7), text="+18", fg="green", bg="white")
                self.attack_bonus_overlay_label.grid(row=current_row, column=current_column, sticky="se", padx=(0,9), pady=(0,4))
            elif field == "geo_damage_percentage":
                self.geo_bonus_overlay_label = tk.Label(menu_frame, font=("Verdana", 7), text="+15", fg="green", bg="white")
                self.geo_bonus_overlay_label.grid(row=current_row, column=current_column, sticky="se", padx=(0,8), pady=(0,4)) 
            
            current_column += 1
            
            if field in ["constellation", "geo_damage_percentage", "max_hp"]:
                current_row += 1
                current_column = 0
                
                if field == "max_hp":
                    ttk.Label(menu_frame, text="Note: Crit stats should be your total before weapon substats are applied."
                              "\n           (ie. the values in character details when a weapon with no crit substat is equipped)",	
                              font=("Helvetica", 9)).grid(row=current_row, column=0, sticky="w", padx=(4,0), pady=(8,2), columnspan=3)
                    current_row += 1
                    
                    tk.Label(menu_frame, text="Artifact Set", font=SECTION_HEADER_FONT).grid(row=current_row, column=0, sticky="w", padx=2, pady=2)
                    current_row += 1
                    current_column = 0
                    
                    artifact_set_frame = tk.Frame(menu_frame)
                    artifact_set_frame.grid(row=current_row, columnspan=4, sticky="nw", pady=(0,2))
                    
                    self.artifact_radio_var = tk.StringVar(artifact_set_frame)
                    self.artifact_radio_var.set("2pc_gladiator/2pc_petra")
                    
                    self.field_var_dict["artifact_set"] = self.artifact_radio_var
                    
                    for i, artifact_set_option in enumerate(["2pc_gladiator/2pc_petra", "4pc_husk", "2pc_petra/2pc_noblesse", "4pc_bolide", "4pc_noblesse", "2pc_petra", 
                                                             "2pc_gladiator/2pc_noblesse", "2pc_gladiator", "2pc_noblesse", "4pc_shimenawa", "none"]):
                        radio_button = tk.Radiobutton(artifact_set_frame, text=artifact_set_option.replace("_", " ").title().replace("Pc", "pc"), variable=self.artifact_radio_var, 
                                                      value=artifact_set_option, command=self.on_artifact_set_change)
                        radio_button.grid(row=current_row, column=current_column, sticky="NW", padx=2)
                        
                        current_column += 1
                        
                        if i == 4:
                            current_row += 1
                            current_column = 0
                    
                    current_row += 1
                    
                    tk.Label(menu_frame, text="Weapon Refinement", font=SECTION_HEADER_FONT).grid(row=current_row, column=0, sticky="w", padx=2, pady=2)
                    current_row += 1
                    current_column = 0
                    
                    self.add_menu_item(menu_frame, current_row, 0, "five_star_refinement", 1)
                    self.add_menu_item(menu_frame, current_row, 1, "four_star_refinement", 1)
                    self.add_menu_item(menu_frame, current_row, 2, "three_star_refinement", 5)
                    
                    in_weapon_section = True
                    current_row += 1
                    
                    tk.Label(menu_frame, text="Weapon Passives", font=SECTION_HEADER_FONT).grid(row=current_row, column=0, sticky="w", padx=2, pady=(8,2))
                    current_row += 1
                    
                    current_row += 1
                    current_column = 0
            elif in_weapon_section and current_column == 4:
                current_row += 1
                current_column = 0
        
        current_row += 1
        self.add_support_buffs_section(self.scrollable_menu_frame, 1)
        
        self.save_defaults()
            
        tk.Button(self.scrollable_menu_frame, text=" ? ", command=self.show_help_window, bg="white", 
                  font=SECTION_HEADER_FONT).grid(row=0, column=0, sticky="NE", pady=(8,0), padx=(8,0), rowspan=2)          
            
        buttons_frame = tk.Frame(self.scrollable_menu_frame)
        buttons_frame.grid(row=2, sticky="news", padx=(8,0), pady=8, columnspan=3)
        buttons_frame.grid_columnconfigure(2, weight=1)
                
        tk.Button(buttons_frame, text="Save Settings", command=self.save_settings, bg="#BCED91", 
                  font=("Helvetica", 13, "bold")).grid(row=0, column=0, sticky="SW")
        
        tk.Button(buttons_frame, text="Restore Defaults", command=self.restore_defaults, bg="#ffd88f", 
                  font=("Helvetica", 13, "bold")).grid(row=0, column=1, sticky="SW", padx=8)
        
        checkbox = tk.Checkbutton(buttons_frame, text="Auto-submit", variable=self.auto_submit_var)
        checkbox.grid(row=0, column=2, sticky="SE", padx=4)
        checkbox.select()
                
        tk.Button(buttons_frame, text="Submit", command=self.submit, bg="#BCED91", 
                  font=("Helvetica", 13, "bold")).grid(row=0, column=3, sticky="SE")
        
    def add_menu_item(self, parent_frame, row, column, field, default, background=None, trailing_pady=0, custom_title=None, columnspan=1):
        if column == 2:
            padx=(4,2)
        else:
            padx = 4
        
        item_subframe = tk.Frame(parent_frame, background=background)
        item_subframe.grid(row=row, column=column, sticky="w", padx=padx, pady=2, columnspan=columnspan)
        
        if custom_title:
            text_field = custom_title
        else:
            text_field = field.replace("_", " ").capitalize()
        
        tk.Label(item_subframe, text=text_field, background=background).grid(row=0, column=0, sticky="w")
        
        var = tk.StringVar()
        
        if field in self.dropdown_dict:
            str_default = str(default)
            
            if str_default == "True":
                var.set("Yes")
            elif str_default == "False":
                var.set("No")
            else:
                var.set(str_default)
            
            dropdown = ttk.Combobox(item_subframe, textvariable=var, state="readonly", width=30, background=background)
            dropdown.grid(row=1, sticky="w", pady=(1,trailing_pady), padx=(4,0))
            dropdown["values"] = self.dropdown_dict[field]
        else:
            var.set(str(default))
            
            textfield = tk.Entry(item_subframe, textvariable=var, width=33, background=background)
            textfield.grid(row=1, padx=(4,0), sticky="w", pady=(0,trailing_pady))
        
        if field in self.help_text_dict:
            position, text = self.help_text_dict[field]
            
            help_button = tk.Button(item_subframe, text="?", command=lambda t=text:self.show_help_window(t), bg="white", font=("Helvetica", 7, "bold"), padx=3)
            help_button.grid(row=0, column=0, padx=(position,0), sticky="NW")
        
        var.trace("w", self.auto_submit)
        
        self.field_var_dict[field] = var
        
    def get_field_value_dict(self):
        field_value_dict = OrderedDict([(field, var.get()) for field, var in self.field_var_dict.iteritems()])
        return field_value_dict
    
    def get_field_value(self, field, is_float=True):
        if is_float:
            return float(self.field_var_dict[field].get())
        else:
            return self.field_var_dict[field].get()
        
    def on_artifact_set_change(self):
        set_attack_bonus, set_geo_bonus, set_normal_bonus, set_charged_bonus, set_ult_bonus = self.get_artifact_set_bonus()
            
        if set_attack_bonus:
            attack_text = "+"+str(int(set_attack_bonus*100.0))
        else:
            attack_text = ""
            
        if set_geo_bonus:
            geo_text = "+"+str(int(set_geo_bonus*100.0))
        else:
            geo_text = ""
            
        self.attack_bonus_overlay_label.configure(text=attack_text)
        self.geo_bonus_overlay_label.configure(text=geo_text)
        
        self.auto_submit()
        
    def get_artifact_set_bonus(self):
        '''Options: "2pc_gladiator/2pc_petra", "2pc_petra/2pc_noblesse", "4pc_noblesse", "4pc_bolide", 
                    "2pc_petra", "2pc_gladiator", "2pc_noblesse", "none"'''
        
        set_attack_bonus = 0
        set_geo_bonus = 0
        set_normal_bonus = 0
        set_charged_bonus = 0
        set_ult_bonus = 0
        
        set_value = self.artifact_radio_var.get() 
        
        if "2pc_gladiator" in set_value:
            set_attack_bonus = 0.18
        elif set_value == "4pc_noblesse":
            set_ult_bonus = 0.20
            set_attack_bonus = 0.20
            
        if "2pc_petra" in set_value:
            set_geo_bonus = 0.15
            
        if "2pc_noblesse" in set_value:
            set_ult_bonus = 0.20
            
        if "4pc_bolide" == set_value:
            set_normal_bonus = 0.4
            set_charged_bonus = 0.4
            
        if "4pc_shimenawa" == set_value:
            set_normal_bonus = 0.5
            set_charged_bonus = 0.5
            
        if "4pc_husk" == set_value:
            set_geo_bonus = 0.24
            
        return set_attack_bonus, set_geo_bonus, set_normal_bonus, set_charged_bonus, set_ult_bonus
        
    def add_support_buffs_section(self, menu_frame, current_row):
        support_buffs_frame = tk.Frame(menu_frame)
        support_buffs_frame.grid(row=current_row, sticky="nw", padx=(4, 0))
        
        tk.Label(support_buffs_frame, text="Support Buffs", font=SECTION_HEADER_FONT).grid(row=0, column=0, sticky="w", padx=2, pady=2)

        # Bennett buff

        bennett_buff_frame = tk.Frame(support_buffs_frame, background=BUFF_FRAME_COLOR, borderwidth=1, relief="solid")
        bennett_buff_frame.grid(row=1, column=0, sticky="NEWS", padx=2)
        
        self.enable_bennett_buff_var = tk.IntVar(self)
        self.enable_bennett_buff_var.trace("w", self.auto_submit)
        self.field_var_dict["enable_bennett_buff"] = self.enable_bennett_buff_var
        
        checkbox = tk.Checkbutton(bennett_buff_frame, text="Bennett Buff", background=BUFF_FRAME_COLOR, activebackground=BUFF_FRAME_COLOR, variable=self.enable_bennett_buff_var)
        checkbox.grid(row=0, column=0, sticky="NW", pady=(2,0), padx=4)
        
        self.enable_bennett_c1_var = tk.IntVar(self)
        self.enable_bennett_c1_var.trace("w", self.auto_submit)
        self.field_var_dict["enable_bennett_c1"] = self.enable_bennett_c1_var        
        
        checkbox = tk.Checkbutton(bennett_buff_frame, text="C1", background=BUFF_FRAME_COLOR, activebackground=BUFF_FRAME_COLOR, variable=self.enable_bennett_c1_var)
        checkbox.grid(row=0, column=1, sticky="NW", pady=(2,0))        
        checkbox.select()
        
        self.add_menu_item(bennett_buff_frame, 1, 0, "bennett_base_attack", 191+674, background=BUFF_FRAME_COLOR, columnspan=2) # Use default value with Festering Desire. 
        self.add_menu_item(bennett_buff_frame, 2, 0, "bennett_talent_level", 13, background=BUFF_FRAME_COLOR, columnspan=2)
        self.add_menu_item(bennett_buff_frame, 3, 0, "bennett_buff_uptime_percentage", 80, background=BUFF_FRAME_COLOR, trailing_pady=4, columnspan=2) # 10 / 15
        
        # Dragonslayer buff
        
        dragonslayer_buff_frame = tk.Frame(support_buffs_frame, background=BUFF_FRAME_COLOR, borderwidth=1, relief="solid")
        dragonslayer_buff_frame.grid(row=1, column=1, sticky="NEWS", padx=2)
        
        self.enable_dragonslayer_var = tk.IntVar(self)
        self.enable_dragonslayer_var.trace("w", self.auto_submit)
        self.field_var_dict["enable_dragonslayer_buff"] = self.enable_dragonslayer_var
        
        checkbox = tk.Checkbutton(dragonslayer_buff_frame, text="Dragonslayer Buff", background=BUFF_FRAME_COLOR, activebackground=BUFF_FRAME_COLOR,
                                  variable=self.enable_dragonslayer_var)
        checkbox.grid(row=0, column=0, sticky="NW", pady=(2,0), padx=4)  
        
        self.add_menu_item(dragonslayer_buff_frame, 1, 0, "dragonslayer_refinement", 5, background=BUFF_FRAME_COLOR)
        self.add_menu_item(dragonslayer_buff_frame, 2, 0, "dragonslayer_buff_uptime_percentage", 50, background=BUFF_FRAME_COLOR) # 10 / 20
        
        # Noblesse buff
        
        noblesse_buff_frame = tk.Frame(support_buffs_frame, background=BUFF_FRAME_COLOR, borderwidth=1, relief="solid")
        noblesse_buff_frame.grid(row=1, column=2, sticky="NEWS", padx=(2,0))
        
        self.enable_support_noblesse_var = tk.IntVar(self)
        self.enable_support_noblesse_var.trace("w", self.submit)
        self.field_var_dict["enable_support_noblesse_buff"] = self.enable_support_noblesse_var
        
        self.add_menu_item(noblesse_buff_frame, 1, 0, "support_noblesse_uptime_percentage", 100, background=BUFF_FRAME_COLOR)
        
        checkbox = tk.Checkbutton(noblesse_buff_frame, text="Support 4pc Noblesse Buff",  background=BUFF_FRAME_COLOR, activebackground=BUFF_FRAME_COLOR,
                                  variable=self.enable_support_noblesse_var)
        checkbox.grid(row=0, column=0, sticky="NW", pady=(2,0), padx=4)
        
    def save_settings(self):
        if not messagebox.askokcancel("Confirm", "Save settings?"):
            return 2
        
        field_value_dict = self.get_field_value_dict()
        
        with open("saved_settings.json", "wb") as output_f:
            json.dump(field_value_dict, output_f)
            
        messagebox.showinfo("Saved", "Settings saved.")
        
    def load_settings(self):
        if os.path.exists("saved_settings.json"):
            try:
                with open("saved_settings.json") as f:
                    field_value_dict = json.load(f)
                
                self.use_settings(field_value_dict)
            except Exception:
                messagebox.showerror("Error", "Saved settings corrupted, restoring defaults.")
                self.restore_defaults(show_ask_message=False)
                
    def use_settings(self, field_value_dict):
        for field, value in field_value_dict.iteritems():
            if field in self.field_var_dict:
                self.field_var_dict[field].set(value)
        
    def save_defaults(self):
        field_value_dict = self.get_field_value_dict()
        
        self.defaults = field_value_dict
            
    def restore_defaults(self, show_ask_message=True):
        if show_ask_message:
            if not messagebox.askokcancel("Confirm", "Restore defaults?"):
                return 2
        
        self.use_settings(self.defaults)
        
        if os.path.exists("saved_settings.json"):
            os.remove("saved_settings.json")
            
        messagebox.showinfo("Restored", "Defaults restored.")
        
    def auto_submit(self, *args):
        if self.auto_submit_var.get():
            self.submit()
        
    def submit(self, *args):
        kwargs = dict()
        
        set_bonus_slots_used = 0
        
        try:
            for field, var in self.field_var_dict.iteritems():
                if field in ["crit_rate", "crit_damage", "artifact_attack_percentage", "geo_damage_percentage"]:
                    value = float(var.get()) / 100.0
                    
                    if field in ["crit_rate", "crit_damage"]:
                        field = "artifact_" + field
                    
                    kwargs[field] = value
                elif field in ["five_star_refinement", "four_star_refinement", "three_star_refinement"]:
                    value = float(var.get())
                    
                    for subfield in self.rarity_refinement_dict[field]:
                        kwargs[subfield] = value
                else:
                    value = var.get()
                    
                    if value == "Yes":
                        value = True
                    elif value == "No":
                        value = False
                    else:
                        try:
                            value = float(value)
                        except ValueError:
                            pass
                        
                    if field == "artifact_set":
                        set_attack_bonus, set_geo_bonus, set_normal_bonus, set_charged_bonus, set_ult_bonus = self.get_artifact_set_bonus()
                        
                        if set_attack_bonus:
                            kwargs["artifact_attack_percentage"] += set_attack_bonus
                            
                        if set_geo_bonus:
                            kwargs["geo_damage_percentage"] += set_geo_bonus
                        
                        kwargs["artifact_normal_bonus"] = set_normal_bonus
                        kwargs["artifact_charged_bonus"] = set_charged_bonus
                        kwargs["artifact_ult_bonus"] = set_ult_bonus
                    elif "bennett" in field:
                        if field == "enable_bennett_buff" and value:
                            bennett_c1_addition = 0.20 * int(self.get_field_value("enable_bennett_c1"))
                            
                            kwargs["artifact_flat_attack"] += self.get_field_value("bennett_base_attack") * \
                                (self.bennet_talent_level_value_dict[self.get_field_value("bennett_talent_level")] + bennett_c1_addition) \
                                * self.get_field_value("bennett_buff_uptime_percentage") / 100.0
                    elif "dragonslayer" in field:
                        if field == "enable_dragonslayer_buff" and value:
                            kwargs["artifact_attack_percentage"] += (0.24 + (self.get_field_value("dragonslayer_refinement")-1) * 0.06) * \
                                self.get_field_value("dragonslayer_buff_uptime_percentage") / 100.0
                    elif "support_noblesse" in field:
                        if field == "enable_support_noblesse_buff" and value and self.artifact_radio_var.get() != "4pc_noblesse":
                            kwargs["artifact_attack_percentage"] += 0.20 * self.get_field_value("support_noblesse_uptime_percentage") / 100.0
                    else:    
                        kwargs[field] = value
                        
                        if field == "artifact_flat_attack": # Hold extra fields that will indicate how much attack can be allowed to rebalance to crit. (done before set bonus applied)
                            if kwargs["artifact_flat_attack"] > 311:
                                kwargs["artifact_substat_flat_attack"] = kwargs["artifact_flat_attack"] - 311.0 
                            else:
                                kwargs["artifact_substat_flat_attack"] = 0.0
                            
                            if kwargs["artifact_attack_percentage"] > 0.466:
                                kwargs["artifact_substat_attack_percentage"] = kwargs["artifact_attack_percentage"] - 0.466
                                
                            else:
                                kwargs["artifact_substat_attack_percentage"] = 0.0 
                                
        except Exception as e:
            for output_text in [self.output_text, self.optimized_stats_output_text]:
                output_text.configure(state="normal")
                output_text.delete(1.0, tk.END)
                output_text.insert(1.0, str(e))
                output_text.configure(state="disabled")
            
            return 0
                
        kwargs["print_parameters"] = False
                
        old_stdout = sys.stdout
              
        for output_text, mode in [(self.output_text, "dps"), (self.optimized_stats_output_text, "optimized")]:
            sys.stdout = stdout_redirect = StringIO()  
            
            kwargs["mode"] = mode
            
            if set_bonus_slots_used > 5:
                print "Warning: Impossible number of set bonuses active.\n"
               
            try:       
                simulate(**kwargs)
                output_value = stdout_redirect.getvalue()
            except Exception as e:
                output_value = str(e)
                
            if self.debug and any([set_attack_bonus, set_geo_bonus, set_normal_bonus, set_charged_bonus, set_ult_bonus]):
                output_value += "\n---------------------------------------------\n\n"
                
                for field_name, value in (("set_attack_bonus", set_attack_bonus), ("set_geo_bonus", set_geo_bonus), ("set_normal_bonus", set_normal_bonus), 
                                          ("set_charged_bonus", set_charged_bonus), ("set_ult_bonus", set_ult_bonus)):
                    output_value += field_name + ": " + str(value) + "\n"
                
            output_text.configure(state="normal")
            output_text.delete(1.0, tk.END)
            output_text.insert(1.0, output_value)
            output_text.configure(state="disabled")
            
        sys.stdout = old_stdout
        
    def show_help_window(self):
        if not self.help_window:
            self.help_window = HelpWindow(self)
            
        self.help_window.deiconify()
        
class HelpWindow(tk.Toplevel):    
    def __init__(self, parent):
        tk.Toplevel.__init__(self, parent)
        
        self.wm_title("Details") 
        self.iconbitmap(ICON_PATH)
        self.protocol("WM_DELETE_WINDOW", self.withdraw)
        
        ttk.Label(self, 
                  text="Details",
                  font=SECTION_HEADER_FONT).grid(padx=5, pady=(5,0), sticky="w")        
        
        ttk.Label(self, 
                  text="Resulting numbers are DPS before defense formulas are applied."
                  "\n\n'DPS (optimized stats)' is the same but with input attack, crit rate and crit damage re-distributed optimally."
                  "\n\n'N normal charged combos' is the number of normal+charged combos done in a full rotation (eg. in 15 seconds)."
                  "\n\n\tMax possible number of combos for each combo_type (based on Dreamy's video) in 12 seconds: "
                  "\n\n\t2_normal: 4.8 combos, 3_normal: 4 combos, 1_normal: 8.6 combos."
                  "\n\n'Combo type' option indicates the number of normal attacks done before a charged attack in a normal+charged combo."
                  "\n\nAt point blank one normal attack can give two star jades if angled correctly, hence the '1_normal_point_blank' combo type option."
                  "\n\n'Full rotation time' is the amount of time it takes to do 'N normal charged combos' 1 or 2 walls (depending on if C2+) and ult.''"
                  "\nThe 15 second setting is for if you plan to always wait for Bennett ult to use ultimate."
                  "\n\n'N ult gems hit' field is for the common case where some ult gems will miss, so one may want to calculate with 11 or 10 "
                  "\ngems instead of 12."
                  "\n\nIf you have questions or feedback, feel free to contact me on Discord (SIVLEOL#4828) or Reddit (u/SIVLEOL).",
                  font=("Helvetica", 9)).grid(padx=5, pady=5, sticky="w")
        
        ttk.Label(self, 
                  text="Credits",
                  font=SECTION_HEADER_FONT).grid(padx=5, pady=(5,0), sticky="w") 
        
        ttk.Label(self, text="Developed and maintained by SIVLEOL#4828"
                  "\n\nSpecial thanks to Meong#2433, Sergio#2336, Zerodyme#9136 and Dreamslayer Ahri#9122 from the Ning discord server for"
                  "\nvaluable testing and feedback.", 
                  font=("Helvetica", 9)).grid(padx=5, pady=5, sticky="w")
        
if __name__ == "__main__":
    app = App()
    app.mainloop()

    # TO DO: Select number of Q jades. Update/add datamined weapons. Make window structure more horizontal.