# Assume everything is level 90 and talent level 10.

import inspect, random

from Ningguang_artifact_simulation import calculate_combo_multiplier
from utils import calculate_crit_multiplier
from stat_balancing import balance_stats

from collections import OrderedDict

NING_BASE_ATTACK_90 = 212
NING_BASE_HP_90 = 9787

HI_SCORE_MODE = False

def get_memory_of_dust_stats(refinement=1, passive_stacks=5, shield_active=True):
    base_attack = 608
    attack_bonus = 0.496
    
    if HI_SCORE_MODE:
        base_attack = 508 # Slightly lower to account for level 80 Ning attack being used.
        attack_bonus = 0.453
    
    passive_attack_bonus = (0.04 + 0.01 * (refinement-1)) * passive_stacks
        
    if shield_active:
        passive_attack_bonus = passive_attack_bonus * 2.0
        
    attack_bonus += passive_attack_bonus
        
    return base_attack, attack_bonus

def get_lost_prayer_stats(refinement=1, passive_stacks=0):    
    base_attack = 608
    elemental_damage_bonus = 0.0
    crit_rate_bonus = 0.331
    
    if passive_stacks:
        elemental_damage_bonus += (0.08 + 0.02 * (refinement-1)) * passive_stacks
        
    return base_attack, elemental_damage_bonus, crit_rate_bonus
        
def get_skyward_atlas_stats(refinement=1, passive_mode="average"):
    base_attack = 674
    attack_bonus = 0.331
    elemental_damage_bonus = 0.12 + 0.03 * (refinement-1)
    
    passive_physical_multiplier = (160 + 40 * (refinement-1)) * 7
    
    if passive_mode == "average":
        passive_physical_multiplier = passive_physical_multiplier / 30.0 # 7 160% procs in 15 seconds, cooldown of 30 seconds. So this gives multiplier per second.  
    elif passive_mode == "on":
        passive_physical_multiplier = passive_physical_multiplier / 15.0
    elif passive_mode == "off":
        passive_physical_multiplier = 0.0
    else:
        raise ValueError("Error: Invalid Skyward Atlas passive mode.")
        
    return base_attack, attack_bonus, elemental_damage_bonus, passive_physical_multiplier

def get_solar_pearl_stats(refinement=1):
    base_attack = 510
    crit_rate_bonus = 0.276
    
    passive_bonus = 0.20 + 0.05 * (refinement-1)
    
    normal_bonus = passive_bonus
    skill_bonus = passive_bonus
    ult_bonus = passive_bonus
    
    return base_attack, crit_rate_bonus, normal_bonus, skill_bonus, ult_bonus

def get_primordial_jade_regalia_stats(refinement=1):
    '''Datamined stats.'''
    base_attack = 674
    crit_damage_bonus = 0.441
    
    HP = 9787 * (1.2 + 0.2 * (refinement-1)) + 4780
    HP_scaling = HP * (0.012 + 0.003 * (refinement-1))
    
    attack_bonus = HP_scaling / (NING_BASE_ATTACK_90 + base_attack) #  HP scaling / base_attack = percentage of attack. 
    
    return base_attack, attack_bonus, crit_damage_bonus

def get_blackcliff_agate_stats(refinement=1, stacks=0):
    base_attack = 510
    crit_damage_bonus = 0.551
    
    attack_bonus = stacks * (0.12 + 0.03 * (refinement-1))
    
    return base_attack, crit_damage_bonus, attack_bonus

def get_mappa_mare_stats(refinement=1, stacks=2):
    base_attack = 565
    elemental_damage_bonus = (0.08 + 0.02 * (refinement-1)) * stacks
    
    return base_attack, elemental_damage_bonus

def get_eye_of_perception_stats(refinement=1, bounces=4):
    base_attack = 454
    attack_bonus = 0.551
    
    passive_physical_multiplier = (240 + 30 * (refinement-1)) * bounces / (12.0 - 1.0 * (refinement-1))
    
    return base_attack, attack_bonus, passive_physical_multiplier

def get_frostbearer_stats(refinement=1, cryo_on_enemy=False):
    base_attack = 510
    attack_bonus = 0.413
    
    if not cryo_on_enemy:
        passive_physical_multiplier = (80 + 15 * (refinement-1)) / 10.0
    else:
        passive_physical_multiplier = (200 + 40 * (refinement - 1)) / 10.0
        
    return base_attack, attack_bonus, passive_physical_multiplier

def get_widsith_stats(refinement=1, buff="average"):
    base_attack = 510
    crit_damage_bonus = 0.551
    
    # Buffs
    attack_bonus = 0.60 + 0.15 * (refinement-1)
    elemental_damage_bonus = 0.48 + 0.12 * (refinement-1)
    
    if buff == "average":
        attack_bonus = attack_bonus / 3.0 / 3.0
        elemental_damage_bonus = elemental_damage_bonus / 3.0 / 3.0
    elif buff == "attack":
        attack_bonus = attack_bonus
        elemental_damage_bonus = 0.0
    elif buff == "elemental damage":
        attack_bonus = 0.0
        elemental_damage_bonus = elemental_damage_bonus
    elif buff == "none":
        attack_bonus = 0.0
        elemental_damage_bonus = 0.0
    else:
        raise ValueError("Non-existent buff mode: " + buff)
    
    return base_attack, crit_damage_bonus, attack_bonus, elemental_damage_bonus

def get_twin_nephrite_stats(refinement=5, passive_active=False):
    base_attack = 448
    crit_rate_bonus = 0.156
    
    if passive_active:
        attack_bonus = 0.12 + 0.02 * (refinement-1)
    else:
        attack_bonus = 0.0
        
    return base_attack, crit_rate_bonus, attack_bonus

def get_favonius_codex_stats(refinement=1):
    base_attack = 510
    
    return base_attack
        
def get_wine_and_song_stats(refinement=1, passive_enabled=True):
    '''Datamined stats.'''
    base_attack = 565
    
    if passive_enabled:
        attack_bonus = (0.20 + 0.05 * (refinement-1)) 
    else:
        attack_bonus = 0.0
    
    return base_attack, attack_bonus

def get_dodoco_tales_stats(refinement=1, charged_passive_enabled=True, attack_passive_enabled=True):
    base_attack = 454
    
    attack_bonus = 0.551 
    
    if charged_passive_enabled:
        charged_bonus = 0.16 + (0.04 * (refinement-1))
    else:
        charged_bonus = 0.0

    if attack_passive_enabled:
        attack_bonus += 0.08 + (0.02 * (refinement-1))
        
    return base_attack, attack_bonus, charged_bonus

def get_royal_grimoire_stats(refinement=1, artifact_crit_rate=0.05, n_simulations=1000):
    base_attack = 565
    attack_bonus = 0.276
    
    n_crits = 0
    crit_stacks = 0
    
    for i in xrange(int(n_simulations)):
        if random.randint(0,99) < artifact_crit_rate * 100.0 + crit_stacks * (8 + 2 * (refinement-1)):
            n_crits += 1
            crit_stacks = 0
        else:
            if crit_stacks < 5:
                crit_stacks += 1
                
    total_crit_rate = n_crits / float(n_simulations)
    
    if artifact_crit_rate > total_crit_rate: # In the case that the crit rate bonus is simulated to be negligible
        total_crit_rate = artifact_crit_rate
    
    crit_rate_bonus = total_crit_rate - artifact_crit_rate
    
    return base_attack, attack_bonus, crit_rate_bonus
        
def get_everlasting_moonglow_stats(refinement=1, max_hp=15822):
    base_attack = 608
    
    max_hp = NING_BASE_HP_90 * 0.496 + max_hp # Add the hp from the weapon passive. 
    
    normal_bonus = (0.01 + 0.0025 * (refinement-1)) * max_hp
    
    return base_attack, normal_bonus
        
def get_hakushin_ring_stats(refinement=1, passive_active=True):
    base_attack = 565
    
    if passive_active:
        elemental_damage_bonus = 0.10 + 0.025 * (refinement-1) 
    else:
        elemental_damage_bonus = 0.0
        
    return base_attack, elemental_damage_bonus

def get_kaguras_verity_stats(refinement=1, passive_stacks=3):
    base_attack = 608
    
    elemental_damage_bonus = 0.12 * passive_stacks
    
    return base_attack, elemental_damage_bonus
    
def get_oathsworn_eye_stats(refinement=1):
    base_attack = 565
    attack_bonus = 0.276
    
    return base_attack, attack_bonus
        
def simulate(talent_level=10, n_normal_charged_combos=4, combo_type="2_normal", constellation=6, full_rotation_time=12,
             artifact_attack_percentage=0.326+0.466, artifact_flat_attack=373, geo_damage_percentage=0.466+0.15+0.24, artifact_crit_rate=0.703, artifact_crit_damage=0.50+0.691, 
             n_ult_gems_hit=12, max_hp=15822, artifact_normal_bonus=0.0, artifact_charged_bonus=0.0, artifact_skill_bonus=0.0, artifact_ult_bonus=0.0,
             memory_of_dust_refinement=1, memory_of_dust_passive_stacks=5, memory_of_dust_shield_active=True, lost_prayer_refinement=1, lost_prayer_passive_stacks=2, 
             skyward_atlas_refinement=1, skyward_atlas_passive_mode="average", everlasting_moonglow_refinement=1, solar_pearl_refinement=1, primordial_jade_regalia_refinement=1, 
             blackcliff_refinement=1, blackcliff_stacks=0, mappa_mare_refinement=1, 
             mappa_mare_stacks=2, eye_of_perception_refinement=1, eye_of_perception_bounces=1, frostbearer_refinement=1, cryo_on_enemy=False, widsith_refinement=1, 
             widsith_buff_mode="average", twin_nephrite_refinement=5, twin_nephrite_passive_active=False, favious_codex_refinement=1, wine_and_song_refinement=1, 
             wine_and_song_passive_active=True, royal_grimoire_refinement=1, royal_grimoire_n_simulations=1000, dodoco_tales_refinement=1, 
             dodoco_tales_charged_passive_enabled=True, dodoco_tales_attack_passive_enabled=True, hakushin_ring_refinement=1, hakushin_ring_passive_active=True, 
             kaguras_refinement=1, kaguras_stacks=3, oathsworn_eye_refinement=1, 
             mode="dps", artifact_substat_attack_percentage=0.326, artifact_substat_flat_attack=62.0, print_parameters=True):
    base_damage_bonus = geo_damage_percentage
    
    if HI_SCORE_MODE:
        talent_level=7
        n_normal_charged_combos=1
        combo_type="1_normal"
        artifact_attack_percentage = 0.8 + 0.18
        artifact_flat_attack = 400
        base_damage_bonus = 0.8
        artifact_crit_rate = 0.5
        artifact_crit_damage = 1.0
    
    damage_score_list = []
    
    # Parameters that will be used by calculate_damage_score every time.
    calc_args = [talent_level, n_normal_charged_combos, combo_type, constellation, full_rotation_time, artifact_attack_percentage, artifact_flat_attack, 
                 base_damage_bonus, artifact_crit_rate, artifact_crit_damage, n_ult_gems_hit, artifact_normal_bonus, artifact_charged_bonus, artifact_skill_bonus, artifact_ult_bonus,
                 mode, artifact_substat_attack_percentage, artifact_substat_flat_attack, damage_score_list]
    
    # --- Memory of Dust ---
    
    base_attack, attack_bonus = get_memory_of_dust_stats(refinement=memory_of_dust_refinement, passive_stacks=memory_of_dust_passive_stacks, shield_active=memory_of_dust_shield_active)
    
    print "Memory of Dust:", calculate_damage_score(base_attack, *calc_args, attack_bonus=attack_bonus)
    
    # --- Lost Prayer ---
    
    base_attack, elemental_damage_bonus, crit_rate_bonus = get_lost_prayer_stats(refinement=lost_prayer_refinement, passive_stacks=lost_prayer_passive_stacks)
    
    print "Lost Prayer:", calculate_damage_score(base_attack, *calc_args, elemental_damage_bonus=elemental_damage_bonus, crit_rate_bonus=crit_rate_bonus)
    
    # --- Skyward Atlas ---
    
    base_attack, attack_bonus, elemental_damage_bonus, passive_physical_multiplier = get_skyward_atlas_stats(refinement=skyward_atlas_refinement, passive_mode=skyward_atlas_passive_mode)
    
    print "Skyward Atlas:", calculate_damage_score(base_attack, *calc_args, attack_bonus=attack_bonus, elemental_damage_bonus=elemental_damage_bonus, 
                                                   weapon_passive_multiplier=passive_physical_multiplier * (1 + artifact_attack_percentage+attack_bonus))    
    
    # --- Everlasting Moonglow ---
    
    base_attack, normal_attack_bonus = get_everlasting_moonglow_stats(refinement=everlasting_moonglow_refinement, max_hp=max_hp)
    
    print "Everlasting Moonglow:", calculate_damage_score(base_attack, *calc_args, normal_attack_bonus=normal_attack_bonus)
    
    # --- Primordial Jade Regalia ---
    
    base_attack, attack_bonus, crit_damage_bonus = get_primordial_jade_regalia_stats(refinement=primordial_jade_regalia_refinement)
    
    print "Primordial Jade Regalia (datamined):", calculate_damage_score(base_attack, *calc_args, attack_bonus=attack_bonus, crit_damage_bonus=crit_damage_bonus)     
    
    # --- Solar Pearl ---
    
    base_attack, crit_rate_bonus, normal_bonus, skill_bonus, ult_bonus = get_solar_pearl_stats(refinement=solar_pearl_refinement)
    
    print "Solar Pearl:", calculate_damage_score(base_attack, *calc_args, crit_rate_bonus=crit_rate_bonus, normal_bonus=normal_bonus, skill_bonus=skill_bonus, ult_bonus=ult_bonus)      
    
    # --- Widsith ---
    
    base_attack, crit_damage_bonus, attack_bonus, elemental_damage_bonus = get_widsith_stats(widsith_refinement, widsith_buff_mode)
    
    print "Widsith:", calculate_damage_score(base_attack, *calc_args, crit_damage_bonus=crit_damage_bonus, attack_bonus=attack_bonus, elemental_damage_bonus=elemental_damage_bonus)    
    
    # --- Dodoco Tales ---

    base_attack, attack_bonus, charged_bonus = get_dodoco_tales_stats(refinement=dodoco_tales_refinement, charged_passive_enabled=dodoco_tales_charged_passive_enabled, 
                                                                      attack_passive_enabled=dodoco_tales_attack_passive_enabled)

    print "Dodoco Tales:", calculate_damage_score(base_attack, *calc_args, attack_bonus=attack_bonus, charged_bonus=charged_bonus)    
    
    # --- Eye of Perception ---
    
    base_attack, attack_bonus, passive_physical_multiplier = get_eye_of_perception_stats(eye_of_perception_refinement, eye_of_perception_bounces)
    
    print "Eye of Perception:", calculate_damage_score(base_attack, *calc_args, attack_bonus=attack_bonus,  
                                                       weapon_passive_multiplier=passive_physical_multiplier * (1 + artifact_attack_percentage+attack_bonus))    
    
    # --- Blackcliff Agate ---
    
    base_attack, crit_damage_bonus, attack_bonus = get_blackcliff_agate_stats(blackcliff_refinement, blackcliff_stacks)
    
    print "Blackcliff Agate:", calculate_damage_score(base_attack, *calc_args, crit_damage_bonus=crit_damage_bonus, attack_bonus=attack_bonus)    
    
    # --- Royal Grimoire ---

    base_attack, attack_bonus, crit_rate_bonus = get_royal_grimoire_stats(refinement=royal_grimoire_refinement, artifact_crit_rate=artifact_crit_rate, 
                                                                          n_simulations=royal_grimoire_n_simulations)
    print "Royal Grimoire:", calculate_damage_score(base_attack, *calc_args, attack_bonus=attack_bonus, crit_rate_bonus=crit_rate_bonus)     
    
    # --- Frostbearer ---
    
    base_attack, attack_bonus, passive_physical_multiplier = get_frostbearer_stats(frostbearer_refinement, cryo_on_enemy)
    
    print "Frostbearer:", calculate_damage_score(base_attack, *calc_args, attack_bonus=attack_bonus,  
                                                 weapon_passive_multiplier=passive_physical_multiplier * (1 + artifact_attack_percentage+attack_bonus))    
    
    # --- Mappa Mare ---
    
    base_attack, elemental_damage_bonus = get_mappa_mare_stats(mappa_mare_refinement, mappa_mare_stacks)
    
    print "Mappa Mare:", calculate_damage_score(base_attack, *calc_args, elemental_damage_bonus=elemental_damage_bonus)     
    
    # --- Wine and Song ---
    
    base_attack, attack_bonus = get_wine_and_song_stats(wine_and_song_refinement, wine_and_song_passive_active)
    
    print "Wine and Song:", calculate_damage_score(base_attack, *calc_args, attack_bonus=attack_bonus)      
    
    # --- Hakushin Ring --- 
    
    base_attack, elemental_damage_bonus = get_hakushin_ring_stats(hakushin_ring_refinement, hakushin_ring_passive_active)
    
    print "Hakushin Ring:", calculate_damage_score(base_attack, *calc_args)
    
    # --- Twin Nephrite ---
    
    base_attack, crit_rate_bonus, attack_bonus = get_twin_nephrite_stats(refinement=twin_nephrite_refinement, passive_active=twin_nephrite_passive_active)
    
    print "Twin Nephrite:", calculate_damage_score(base_attack, *calc_args, crit_rate_bonus=crit_rate_bonus, attack_bonus=attack_bonus)
    
    # --- Favonius Codex ---
    
    base_attack = get_favonius_codex_stats(favious_codex_refinement)
    
    print "Favonius Codex/Prototype Amber:", calculate_damage_score(base_attack, *calc_args)

    # --- Kagura's Verity ---
    base_attack, elemental_damage_bonus = get_kaguras_verity_stats(kaguras_refinement, kaguras_stacks)

    print "Kagura's Verity (datamined):", calculate_damage_score(base_attack, *calc_args, elemental_damage_bonus=elemental_damage_bonus)

    # --- Oathsworn's Eye ---
    base_attack, attack_bonus = get_oathsworn_eye_stats(oathsworn_eye_refinement)
    
    print "Oathsworn's Eye (datamined):", calculate_damage_score(base_attack, *calc_args, attack_bonus=attack_bonus)

    if print_parameters:
        print "\n----------------------------\n\nParameters:", [(arg, locals()[arg]) for arg in inspect.getargspec(simulate).args]

    return damage_score_list

def calculate_damage_score(base_attack, talent_level, n_normal_charged_combos, combo_type, constellation, full_rotation_time, 
                           artifact_attack_percentage, artifact_flat_attack, base_damage_bonus, artifact_crit_rate, artifact_crit_damage, n_ult_gems_hit, artifact_normal_bonus, 
                           artifact_charged_bonus, artifact_skill_bonus, artifact_ult_bonus, mode="dps", artifact_substat_attack_percentage=0.0, 
                           artifact_substat_flat_attack=0.0, damage_score_list=None, attack_bonus=0.0, elemental_damage_bonus=0.0, crit_rate_bonus=0.0, crit_damage_bonus=0.0, 
                           weapon_passive_multiplier=0.0, normal_bonus=0.0, charged_bonus=0.0, skill_bonus=0.0, ult_bonus=0.0, normal_attack_bonus=0.0):
    '''Modes:
    - dps: Give DPS 
    - optimized: Give DPS with optimized stats.
    
    artifact_substat_attack_percentage and artifact_substat_flat_attack are for "optimized" mode determination of minimum_attack_percentage.
    '''    
    if damage_score_list is None:
        damage_score_list = []
    
    base_attack, artifact_attack_percentage = calculate_attack_stats(base_attack, artifact_attack_percentage, artifact_flat_attack)
    
    normal_attack_bonus = float(normal_attack_bonus) / base_attack
    
    if mode == "optimized":
        minimum_attack_percentage = artifact_attack_percentage+attack_bonus - ((artifact_substat_flat_attack / float(base_attack)) + artifact_substat_attack_percentage)
        
        # Balance the stats optimally and move all the values into the "bonus" parameters.
        attack_bonus, crit_rate_bonus, crit_damage_bonus = balance_stats(artifact_attack_percentage+attack_bonus, artifact_crit_rate+crit_rate_bonus, 
                                                                         artifact_crit_damage+crit_damage_bonus, minimum_attack_percentage=minimum_attack_percentage)
        
        artifact_attack_percentage = 0.0
        artifact_crit_rate = 0.0
        artifact_crit_damage = 0.0
    elif mode == "dps":
        pass 
    else:
        raise ValueError("Error: Unexpected mode: " + str(mode))
    
    combo_multiplier = (calculate_combo_multiplier(talent_level=talent_level, n_normal_charged_combos=n_normal_charged_combos, combo_type=combo_type, 
                                                   constellation=constellation, atk_bonus=artifact_attack_percentage+attack_bonus, 
                                                   dmg_bonus=base_damage_bonus+elemental_damage_bonus, normal_bonus=artifact_normal_bonus+normal_bonus, 
                                                   charged_bonus=artifact_charged_bonus+charged_bonus, skill_bonus=artifact_skill_bonus+skill_bonus, 
                                                   ult_bonus=artifact_ult_bonus+ult_bonus, full_rotation_time=full_rotation_time, 
                                                   n_ult_gems_hit=n_ult_gems_hit, normal_attack_bonus=normal_attack_bonus) + weapon_passive_multiplier)
    crit_multiplier = calculate_crit_multiplier(artifact_crit_rate+crit_rate_bonus, artifact_crit_damage+crit_damage_bonus)
    
    damage_score = base_attack * combo_multiplier / 100.0 * crit_multiplier
    
    if HI_SCORE_MODE and base_attack == 720:
        damage_score = damage_score * 15.0
    
    if mode == "dps":
        final_damage_score = int(round(damage_score))
    elif mode == "optimized":
        final_damage_score = str(int(round(damage_score))) + "\nAtk=%.2f C.rate=%.2f C.damage=%.2f\n" % (base_attack*(1.0+attack_bonus), crit_rate_bonus*100.0, crit_damage_bonus*100.0)
    else:
        raise ValueError("Error: Unexpected mode: " + str(mode))
    
    damage_score_list.append(final_damage_score)
    
    return final_damage_score
   
def calculate_attack_stats(weapon_base_attack, artifact_attack_percentage, artifact_flat_attack):
    '''Add on character base attack and combine flat attack into the attack percentage.'''
    base_attack = weapon_base_attack + NING_BASE_ATTACK_90
    artifact_attack_percentage += artifact_flat_attack / float(base_attack)
    
    return base_attack, artifact_attack_percentage   
    
def main(four_star_refinement=5, five_star_refinement=1, three_star_refinement=5):
    simulate(talent_level=6, n_normal_charged_combos=5, combo_type="2_normal", constellation=0, memory_of_dust_refinement=five_star_refinement, memory_of_dust_passive_stacks=5, 
             memory_of_dust_shield_active=True, lost_prayer_refinement=five_star_refinement, lost_prayer_passive_stacks=2, skyward_atlas_refinement=five_star_refinement, 
             skyward_atlas_passive_mode="average", everlasting_moonglow_refinement=five_star_refinement, solar_pearl_refinement=four_star_refinement, 
             primordial_jade_regalia_refinement=five_star_refinement, blackcliff_refinement=four_star_refinement, blackcliff_stacks=0, 
             mappa_mare_refinement=four_star_refinement, mappa_mare_stacks=2, eye_of_perception_refinement=four_star_refinement, eye_of_perception_bounces=1, 
             frostbearer_refinement=four_star_refinement, cryo_on_enemy=False, widsith_refinement=four_star_refinement, widsith_buff_mode="average",
             twin_nephrite_refinement=three_star_refinement, twin_nephrite_passive_active=False, favious_codex_refinement=four_star_refinement,
             wine_and_song_refinement=four_star_refinement, wine_and_song_passive_active=True, royal_grimoire_refinement=four_star_refinement, royal_grimoire_n_simulations=1000,
             dodoco_tales_refinement=four_star_refinement, dodoco_tales_charged_passive_enabled=True, dodoco_tales_attack_passive_enabled=True,
             hakushin_ring_refinement=four_star_refinement, hakushin_ring_passive_active=True, kaguras_refinement=five_star_refinement, kaguras_stacks=3, 
             oathsworn_eye_refinement=four_star_refinement)
    
if __name__ == "__main__":
    main(four_star_refinement=1, five_star_refinement=1, three_star_refinement=5)
    
    
    # TODO: Add refinement comparison.