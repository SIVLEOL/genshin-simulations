Some Genshin related math scripts I wrote are here.

# Ningguang Weapon comparison script

The most useful and detailed script here is the Ningguang weapon comparison script, which now has a GUI. It looks like this:

![GUI Screenshot](https://bitbucket.org/SIVLEOL/genshin-simulations/raw/b52132211f52ac10b49aa6a22eae2a06e2e712aa/screenshots/GUI_screenshot.png)

## Usage

To run the comparison with different stats/options than the above default values (based on my current substats), there are the below options.

### Executable version

A standalone .exe version is provided: 

[Click here to download](https://bitbucket.org/SIVLEOL/genshin-simulations/raw/08a7b34ab42aa3f8ff24122af3b876320bcd9ece/dist/weapon_comparison_gui.exe)

Note: Windows may find the above .exe sketchy since not many people use it, you can go ahead run it anyways.

### Python 2 

Alternatively, the Python 2 source code is available in this repo. The only dependency is Python 2 itself. 

With Python 2 installed you can clone the repo and either:

- Run the GUI version: 
python weapon_comparison_GUI.py

- Run the comparison script directly:
python Ningguang_weapon_comparison.py

### Older Versions

I'll put up a list of the older versions later (though you can find them in the repo history for now if needed).